<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BeaconWatcher API Documentation</title>
<script src="../manager/js/jquery-1.10.2.min.js"></script>
<!--<script src="https://beaconwatcher-admin.3scale.net/assets/active-docs/application.js" ></script>
<link href="https://beaconwatcher-admin.3scale.net/assets/active-docs/application.css" media="screen" rel="stylesheet" />-->
<script src="https://beaconwatcher.3scale.net/assets/active-docs/application.js" ></script>
<link href="https://beaconwatcher.3scale.net/assets/active-docs/application.css" media="screen" rel="stylesheet" />
<style>
table.codes{background-color:#CCC;}
table.codes th{background-color:#999;}
table.codes td{background-color:#FFF;}
</style>
</head>

<body>
<h1>Introduction</h1>
<p>The following document describes BeaconWatcher JSON-RPC API resources. The API is based under URL http://api.beaconwatcher.com, secured with SSL. Every request to the API should contain the secret App Key which can be found in our Online Manager.</p>
<h1>About JSON-RPC</h1>
<p>JSON-RPC is a stateless, light-weight remote procedure call (RPC) protocol. Primarily this specification defines several data structures and the rules around their processing. It is transport agnostic in that the concepts can be used within the same process, over sockets, over http, or in many various message passing environments. It uses <a href="http://www.json.org" target="_blank">JSON</a> as data format.</p>
<p><strong>It is designed to be simple!</strong></p>
<h1>Conventions</h1>
<p>The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in <a class="reference" href="http://www.ietf.org/rfc/rfc2119.txt" target="_blank">RFC 2119</a>.</p>
<p>Since JSON-RPC uses JSON, it shares the same type system as JSON (see <a class="reference" href="http://www.json.org" target="_blank">http://www.json.org</a> or <a class="reference" href="http://www.ietf.org/rfc/rfc4627.txt" target="_blank">RFC 4627</a>). Whenever this document refers to any JSON type, the first letter is always capitalized: Object, Array, String, Number, True, False, Null.</p>
<p>Clients are the origin of Request objects. Servers are the origin of Response objects.</p>
<h1>HTTP Header</h1>
<p>Requirements:</p>
<p>Regardless of whether a remote procedure call is made using HTTP GET or POST, the HTTP request message MUST specify the following headers:</p>
<ul class="simple">
	<li>Content-Type SHOULD be 'application/json-rpc' but MAY be 'application/json' or 'application/jsonrequest'</li>
	<li>The Content-Length MUST be specified and correct according to the guidelines and rules laid out in Section 4.4, Message Length, of the <a class="reference" href="http://www.ietf.org/rfc/rfc2616.txt">HTTP</a> specification.</li>
	<li>The Accept MUST be specified and SHOULD read 'application/json-rpc' but MAY be 'application/json' or 'application/jsonrequest'.</li>
</ul>
<h1>Response Codes</h1>
<h2>Success</h2>
<p>Requests using POST or GET MUST indicate a success response using HTTP status code: 200.</p>
<h2>Errors</h2>
<p>
<table align="left" border="0" cellpadding="2" cellspacing="1" class="codes">
	<tbody valign="top">
		<tr>
			<th width="150" align="left">HTTP Status</th>
			<th width="150" align="left">code</th>
			<th width="200" align="left">message</th>
		</tr>
		<tr>
			<td>500</td>
			<td>-32700</td>
			<td>Parse error.</td>
		</tr>
		<tr>
			<td>400</td>
			<td>-32600</td>
			<td>Invalid Request.</td>
		</tr>
		<tr>
			<td>404</td>
			<td>-32601</td>
			<td>Method not found.</td>
		</tr>
		<tr>
			<td>500</td>
			<td>-32602</td>
			<td>Invalid params.</td>
		</tr>
		<tr>
			<td>500</td>
			<td>-32603</td>
			<td>Internal error.</td>
		</tr>
		<tr>
			<td>500</td>
			<td>-32099..-32000</td>
			<td>Server error.</td>
		</tr>
	</tbody>
</table>
</p>
<div style="clear:both; height:15px;"></div>
<div class='api-docs-wrap'></div>
<script>
	$(function(){
		// if you are hosting you specs elsewhere, load them by:
		//ThreeScale.APIDocs.host = 'https://beaconwatcher-admin.3scale.net';
		ThreeScale.APIDocs.host = 'https://beaconwatcher.3scale.net';
		ThreeScale.APIDocs.init([]);
	});
</script>
</body>
</html>