$(function() {
	$('a.cnt').live('click', function(e) {
		var href = $(this).attr("href");
		var myFilename = getPageName(href);
		loadContent("pages/"+myFilename);
		e.preventDefault();
	});
});

function loadContent(url) {
	$('#load_contents').load(url);
}

function getPageName(url) {
	var index = url.lastIndexOf("/") + 1;
	var filenameWithExtension = url.substr(index);
	return filenameWithExtension;
	//var filename = filenameWithExtension.split(".")[0]; // <-- added this line
	//return filename; // <-- added this line
}
