{
 "basePath": "http://api.beaconwatcher.com/index.php",
 "apiVersion": "v1",
 "apis": [
   {
     "path": "/",
     "operations": [
       {
        "httpMethod": "POST",
         "summary": "Get Sites",
         "description": "<p>This operation is used to get the List of sites against specific App.</p>",
         "group": "Beacons",
         "parameters": [
           {
             "name": "action",
             "description": "getSites",
             "dataType": "string",
             "paramType": "query",
             "threescale_name": "action"
           },
		   {
             "name": "key",
             "description": "App Key",
             "dataType": "string",
             "paramType": "query",
             "threescale_name": "key"
           }
         ]
       }
     ]
   }
 ]
}