<?php

class Actions {
    public function getSiteStatus() {
        return array('status' => 'healthy',
            'time' => date('d-M-Y H:i'));
    }

    public function addTwoNumbers($params) {
        return array('result' => ($params['a'] + $params['b']));
    }
	
	public function login($params) {
		$apk_id = chkAppKey($params['key']);
		/*if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{*/
			//$rs = mysql_query("SELECT * FROM members WHERE mem_password='".md5($params['pass'])."' AND mem_login='".$params['email']."' AND mem_isadmin=0 AND apk_id='".$apk_id."'") or die(mysql_error());// die(array('status' => 0, 'message' => mysql_error()));
			//$rs = mysql_query("SELECT * FROM members WHERE mem_password='".md5($params['pass'])."' AND mem_login='".$params['email']."' AND apk_id='".$apk_id."'") or die(mysql_error());// die(array('status' => 0, 'message' => mysql_error()));
			$rs = mysql_query("SELECT * FROM members WHERE mem_password='".md5($params['pass'])."' AND mem_login='".$params['email']."'") or die(mysql_error());// die(array('status' => 0, 'message' => mysql_error()));
			if(mysql_num_rows($rs)>0){
				$row = mysql_fetch_object($rs);
				$retValue = array('status' => 1, 'message' => 'Logged In',
					'data' => array('userID' => $row->mem_id, 'first_name' => $row->mem_fname, 'last_name' => $row->mem_lname)
				);
				$retValue['data']['apps'] = getApps($row->mem_id, $row->utype_id, $apk_id);
				if($row->utype_id==4){
					//$retValue['data']['sites'] = getSites($row->mem_id, $row->apk_id);
				}
				else{
					//$retValue['data']['sites'] = getSites($row->mem_id, 0);
				}			
				//$retValue['data']['countries'] = getCountries();
			}
			else{
				$retValue = array('status' => 0, 'message' => 'Invalid Username or Password');
			}
		//}
		return $retValue;
    }
	
	public function register($params) {
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			if(IsExist("mem_id", "members", "mem_login", $params['email'])){
				$retValue = array('status' => 0, 'message' => 'Username / Email already exist');
			}
			else{
				$memid = getMaximum("members","mem_id");
				//mysql_query("INSERT INTO members(mem_id, mem_login, mem_password, mem_fname, mem_lname, mem_company, mem_datecreated, mem_confirm, status_id, mem_isadmin, utype_id) VALUES(".$memid.", '".$params['email']."', '".md5($params['pass'])."', '".$params['fname']."', '".$params['lname']."', '".$params['mem_company']."', '".date("Y-m-d")."', '1', '1', '0', '3')");
				mysql_query("INSERT INTO members(mem_id, mem_login, mem_password, mem_fname, mem_lname, mem_company, mem_datecreated, mem_confirm, status_id, mem_isadmin, utype_id, apk_id) VALUES(".$memid.", '".$params['email']."', '".md5($params['pass'])."', '".$params['fname']."', '".$params['lname']."', '".$params['mem_company']."', '".date("Y-m-d")."', '1', '1', '0', '4', '".$apk_id."')");
				$retValue = array('status' => 1, 'message' => 'Account created',
					'data' => array('userID' => $memid, 'first_name' => $params['fname'], 'last_name' => $params['lname'])
				);
				$retValue['data']['sites'] = array();
				$retValue['data']['countries'] = getCountries();
			}
		}
		return $retValue;
    }
	
	public function addSite($params) {
		//$isValidKey = chkAppKey($params['key'], $params['app_id']);
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			$siteid = getMaximum("mem_sites","site_id");
			$cntName = returnName("countries_name", "countries", "countries_id", $params['country_id']);
			$address = $params['site_address'].",".$params['site_city'].",".$params['site_state'].",".$cntName.",".$params['site_pcode'];
			$loc = getLnt($address);
			$lat = $loc['lat'];
			$lon = $loc['lng'];
			mysql_query("INSERT INTO mem_sites(site_id, mem_id, apk_id, site_title, site_details, site_phone, site_mobile, site_address, site_city, site_state, site_pcode, countries_id, site_datecreated, status_id, site_long, site_lat) VALUES(".$siteid.", '".$params['userID']."', '".$apk_id."', '".$params['site_title']."', '".$params['site_details']."', '".$params['site_phone']."', '".$params['site_mobile']."', '".$params['site_address']."', '".$params['site_city']."', '".$params['site_state']."', '".$params['site_pcode']."', '".$params['country_id']."', '".date("Y-m-d")."', '1', '".$lon."', '".$lat."')") or die(mysql_error());
			$retValue = array('status' => 1, 'message' => 'Site Added',
				'data' => array('userID' => $params['userID'])
			);
			$retValue['data']['sites'] = getSites($params['userID'], $apk_id);
		}
		return $retValue;
    }
	
	public function getSites($params) {
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			$userID = 0;
			if(isset($params['userID'])){
				if($params['userID']>0){
					$userID = $params['userID'];
				}
			}
			if($userID > 0){
				$strQry = "SELECT site_id, site_title FROM mem_sites WHERE mem_id=".$userID;
			}
			else{
				$strQry = "SELECT site_id, site_title FROM mem_sites WHERE apk_id=".$apk_id;	
			}
			$rs1 = mysql_query($strQry);
			if(mysql_num_rows($rs1)>0){
				$retValue = array('status' => 1, 'message' => 'Site List');
				while($row1=mysql_fetch_object($rs1)){
					$retValue['data'][] = array('site_id'=>$row1->site_id, 'site_name'=>$row1->site_title); 
				}
			}
			else{
				$retValue = array('status' => 0, 'message' => 'No record found');
			}
			/*$retValue = array('status' => 1, 'message' => 'Site List');
			$userID = 0;
			if(isset($params['userID'])){
				if($params['userID']>0){
					$userID = $params['userID'];
				}
			}
			$retValue['data'] = getSites($userID, $row->apk_id);*/
		}
		return $retValue;
    }
	
	public function getCategories($params) {
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			$rs = mysql_query("SELECT * FROM categories ORDER BY cat_name") or die(array('status' => 0, 'message' => mysql_error()));
			if(mysql_num_rows($rs)>0){				
				$retValue = array('status' => 1, 'message' => 'Categories List');
				while($row = mysql_fetch_object($rs)){
					$retValue['data'][] = array('cat_id' => $row->cat_id, 'cat_name' => $row->cat_name);
				}				
			}
			else{
				$retValue = array('status' => 0, 'message' => 'No record found');
			}
		}
		return $retValue;
    }
	
	public function getBeacons($params) {
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			$rs = mysql_query("SELECT b.*, s.site_id, s.site_title, m.mem_company FROM msite_beacons AS b LEFT OUTER JOIN mem_sites AS s ON s.site_id=b.site_id LEFT OUTER JOIN members AS m ON m.mem_id=s.mem_id WHERE b.apk_id='".$apk_id."' AND b.site_id='".$params['site_id']."'") or die(mysql_error());// die(array('status' => 0, 'message' => mysql_error()));
			if(mysql_num_rows($rs)>0){				
				$retValue = array('status' => 1, 'message' => 'Beacon details');
				while($row = mysql_fetch_object($rs)){
					$retValue['data'][] = array('msb_id' => $row->msb_id, 'msb_name' => $row->msb_name, 'msb_location' => $row->msb_location, 'msb_uuid' => $row->msb_uuid, 'msb_minor' => $row->msb_minor, 'msb_major' => $row->msb_major, 'msb_txpower' => $row->msb_txpower, 'msb_mac_address' => $row->msb_mac_address, 'site_id' => $row->site_id, 'site_title' => $row->site_title, 'company' => $row->mem_company);
				}				
			}
			else{
				$retValue = array('status' => 0, 'message' => 'No record found');
			}
		}
		return $retValue;
    }
	
	public function getBeacon($params) {
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			$rs = mysql_query("SELECT b.*, s.site_id, s.site_title, m.mem_company FROM msite_beacons AS b LEFT OUTER JOIN mem_sites AS s ON s.site_id=b.site_id LEFT OUTER JOIN members AS m ON m.mem_id=s.mem_id WHERE b.msb_uuid='".$params['uuid']."'") or die(mysql_error());// die(array('status' => 0, 'message' => mysql_error()));
			if(mysql_num_rows($rs)>0){
				
				$retValue = array('status' => 1, 'message' => 'Beacon details');
				while($row = mysql_fetch_object($rs)){
					$retValue['data'][] = array('msb_id' => $row->msb_id, 'msb_name' => $row->msb_name, 'msb_location' => $row->msb_location, 'msb_uuid' => $row->msb_uuid, 'msb_minor' => $row->msb_minor, 'msb_major' => $row->msb_major, 'msb_mac_address' => $row->msb_mac_address, 'msb_txpower' => $row->msb_txpower, 'site_id' => $row->site_id, 'site_title' => $row->site_title, 'company' => $row->mem_company);
				}				
			}
			else{
				$retValue = array('status' => 0, 'message' => 'No record found');
			}
		}
		return $retValue;
    }
	
	public function addBeacon($params) {
		//$isValidKey = chkAppKey($params['key'], $params['app_id']);
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			//if(IsExist("msb_id", "msite_beacons", "msb_uuid", $params['uuid']."' AND msb_minor='".$params['msb_minor']."' AND msb_major='".$params['msb_major']."'")){
			if(IsExist("msb_id", "msite_beacons", "msb_uuid", $params['uuid']."' AND msb_minor='".$params['msb_minor']."' AND msb_major='".$params['msb_major']."' AND site_id='".$params['site_id'])){
				$retValue = array('status' => 0, 'message' => 'Beacon already exist!');
			}
			else{
				$maxID = getMaximum("msite_beacons","msb_id");
				//$strQuery = "INSERT INTO msite_beacons(msb_id, site_id, msb_name, msb_details, msb_location, msb_uuid, msb_datecreated, status_id, msb_minor, msb_major, msb_mac_address, msb_txpower) VALUES(".$maxID.", '".$params['site_id']."', '".$params['name']."', '".$params['details']."', '".$params['location']."', '".$params['uuid']."', '".date("Y-m-d")."', '1', '".$params['msb_minor']."', '".$params['msb_major']."', '".$params['msb_mac_address']."', '".$params['msb_txpower']."')";
				mysql_query("INSERT INTO msite_beacons(msb_id, site_id, msb_name, msb_details, msb_location, msb_uuid, msb_datecreated, status_id, msb_minor, msb_major, msb_mac_address, msb_txpower, apk_id) VALUES(".$maxID.", '".$params['site_id']."', '".$params['name']."', '".$params['details']."', '".$params['location']."', '".$params['uuid']."', '".date("Y-m-d")."', '1', '".$params['msb_minor']."', '".$params['msb_major']."', '".$params['msb_mac_address']."', '".$params['msb_txpower']."', '".$apk_id."')");
				$retValue = array('status' => 1, 'message' => 'Beacon Added',
					'data' => array('msb_id' => $maxID, 'msb_name' => $params['name'])
				);
			}
		}
		return $retValue;
    }
	
	public function getNotifications($params) {
		//$isValidKey = chkAppKey($params['key'], $params['app_id']);
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			$isNew = 0;
			$ntBeaconExist = 0;
			$msb_id = chkExist("msb_id", "msite_beacons", "WHERE msb_uuid='".$params['msb_uuid']."' AND msb_minor='".$params['msb_minor']."' AND msb_major='".$params['msb_major']."'");
			if($msb_id==0){
				$msb_id = getMaximum("msite_beacons","msb_id");
				mysql_query("INSERT INTO msite_beacons(msb_id, site_id, msb_name, msb_details, msb_location, msb_datecreated, status_id, msb_uuid, msb_minor, msb_major, apk_id, msb_mac_address, msb_txpower) VALUES(".$msb_id.", '0', 'Default_".$msb_id."', '', '', '".date("Y-m-d")."', '1', '".$params['msb_uuid']."', '".$params['msb_minor']."', '".$params['msb_major']."', '".$apk_id."', '".$params['msb_mac_address']."', '".$params['msb_txpower']."')") or die(mysql_error());
				$isNew = 1;	
			}
			else{
				$rs=mysql_query("SELECT * FROM notification_beacons WHERE msb_id=".$msb_id);
				if(mysql_num_rows($rs)>0){
					$ntBeaconExist = 1;
				} else{
					$site_id = chkExist("site_id", "msite_beacons", "WHERE msb_uuid='".$params['msb_uuid']."' AND msb_minor='".$params['msb_minor']."' AND msb_major='".$params['msb_major']."'");
					if($site_id==0){
						$isNew = 1;
					}
					else{
						$ntSiteExist = 1;	
					}
				}
			}
			
			/*$rs = mysql_query("SELECT mb.* FROM msite_beacons AS mb LEFT OUTER JOIN  WHERE mb.msb_id=".$msb_id);
			$row = mysql_fetch_object($rs);
			$site_id = $row->site_id;*/
			
			if($isNew==1){
				$cnter = 0;
				$ids = 0;
				$qry1 = mysql_query("SELECT nt_id FROM notification_apps WHERE apk_id=".$apk_id." AND nt_id NOT IN (SELECT nt_id FROM notification_beacons) AND  nt_id NOT IN (SELECT nt_id FROM notification_sites)");
				if(mysql_num_rows($qry1)){
					while($idr=mysql_fetch_object($qry1)){
						if($cnter>0){
							$ids .= ",";
						}
						$ids .= $idr->nt_id;
						$cnter++;
					}
				}
				$strQuery = "SELECT n.*, z.nzones_id, z.nzones_name, t.tpl_name FROM notifications AS n 
				LEFT OUTER JOIN templates AS t ON t.tpl_id=n.tpl_id 
				LEFT OUTER JOIN notification_zones AS z ON z.nzones_id=n.nzone_id
				WHERE n.status_id=1 AND (nt_startdate <= '".date("Y-m-d")."' AND nt_enddate >= '".date("Y-m-d")."') 
				AND n.nt_id IN (".$ids.")";
			}
			else{
				if($ntBeaconExist==1){
					$strQuery = "SELECT n.*, b.msb_id, z.nzones_id, z.nzones_name, t.tpl_name FROM notifications AS n 
					INNER JOIN notification_beacons AS b ON b.nt_id=n.nt_id AND b.msb_id=".$msb_id." 
					LEFT OUTER JOIN templates AS t ON t.tpl_id=n.tpl_id 
					LEFT OUTER JOIN notification_zones AS z ON z.nzones_id=n.nzone_id
					WHERE n.status_id=1 AND (nt_startdate <= '".date("Y-m-d")."' AND nt_enddate >= '".date("Y-m-d")."') 
					AND n.nt_id IN (SELECT nt_id FROM notification_apps WHERE apk_id=".$apk_id.")";
				}
				else{
					$strQuery = "SELECT n.*, b.msb_id, z.nzones_id, z.nzones_name, t.tpl_name FROM notifications AS n 
					INNER JOIN notification_sites AS s ON s.nt_id=n.nt_id AND s.site_id=".$site_id." 
					LEFT OUTER JOIN templates AS t ON t.tpl_id=n.tpl_id 
					LEFT OUTER JOIN notification_zones AS z ON z.nzones_id=n.nzone_id
					WHERE n.status_id=1 AND (nt_startdate <= '".date("Y-m-d")."' AND nt_enddate >= '".date("Y-m-d")."') 
					AND n.nt_id IN (SELECT nt_id FROM notification_apps WHERE apk_id=".$apk_id.")";
				}
			}	
			//print($strQuery);
			$rs = mysql_query($strQuery) or die(mysql_error()); // die(array('status' => 0, 'message' => mysql_error()));
			if(mysql_num_rows($rs)>0){
				$retValue = array('status' => 1, 'message' => 'Notification');
				while($row = mysql_fetch_object($rs)){
					//$nt_url = "";
					//if($row->tpl_id>0){
						$nt_url = "http://beaconwatcher.com/api/template.php?nt_id=".$row->nt_id;
					//}
					//$retValue['data'][] = array('nt_id' => $row->nt_id, 'nt_name' => $row->nt_name, 'nt_title' => $row->nt_title, 'nt_details' => $row->nt_details, 'zone' => $row->nzones_id, 'template' => $row->tpl_name);
					$retValue['data'][] = array('nt_id' => $row->nt_id, 'nt_name' => $row->nt_name, 'nt_title' => $row->nt_title, 'nt_details' => $row->nt_details, 'zone' => $row->nzones_id, 'template' => $nt_url);
				}
			}
			else{
				$retValue = array('status' => 0, 'message' => 'No record found');
			}
		}
		return $retValue;
    }
	
	public function getAllNotifications($params) {
		$apk_id = chkAppKey($params['key']);
		if($apk_id==0){
			$retValue = array('status' => 0, 'message' => 'Wrong API Key!');
		}
		else{
			$catQuery = "";
			if(strlen($params['cat_id'])>0){
				//$catQuery = "AND n.cat_id='".$params['cat_id']."'";
				$catQuery = "AND n.cat_id IN (".$params['cat_id'].")";
			}
			
			//if(isset($params['site_id'])){
			if(isset($params['userID'])){
				//$strQuery = "SELECT b.*, s.site_id, s.site_title, m.mem_company FROM msite_beacons AS b LEFT OUTER JOIN mem_sites AS s ON s.site_id=b.site_id LEFT OUTER JOIN members AS m ON m.mem_id=s.mem_id WHERE b.apk_id='".$apk_id."' AND b.site_id='".$params['site_id']."'";
				$strQuery = "SELECT b.*, s.site_id, s.site_title, m.mem_company FROM msite_beacons AS b LEFT OUTER JOIN mem_sites AS s ON s.site_id=b.site_id LEFT OUTER JOIN members AS m ON m.mem_id=s.mem_id WHERE b.apk_id='".$apk_id."' AND s.mem_id='".$params['userID']."'";
			}
			else{
				$strQuery = "SELECT b.*, s.site_id, s.site_title, m.mem_company FROM msite_beacons AS b LEFT OUTER JOIN mem_sites AS s ON s.site_id=b.site_id LEFT OUTER JOIN members AS m ON m.mem_id=s.mem_id WHERE b.apk_id='".$apk_id."'";
			}
			//print($strQuery);
			$rs = mysql_query($strQuery) or die(mysql_error());
			if(mysql_num_rows($rs)>0){				
				$retValue = array('status' => 1, 'message' => 'Beacon details');
				while($row = mysql_fetch_object($rs)){
					$noti = array();
					//$retValue['data'][] = array('msb_id' => $row->msb_id, 'msb_name' => $row->msb_name, 'msb_location' => $row->msb_location, 'msb_uuid' => $row->msb_uuid, 'msb_minor' => $row->msb_minor, 'msb_major' => $row->msb_major, 'msb_txpower' => $row->msb_txpower, 'msb_mac_address' => $row->msb_mac_address, 'site_id' => $row->site_id, 'site_title' => $row->site_title, 'company' => $row->mem_company);
					if(isset($params['userID'])){
						$strQuery1 = "SELECT n.*, b.msb_id, z.nzones_id, z.nzones_name, t.tpl_name FROM notifications AS n 
						INNER JOIN notification_beacons AS b ON b.nt_id=n.nt_id AND b.msb_id=".$row->msb_id." 
						LEFT OUTER JOIN templates AS t ON t.tpl_id=n.tpl_id 
						LEFT OUTER JOIN notification_zones AS z ON z.nzones_id=n.nzone_id
						WHERE n.status_id=1 ".$catQuery." AND (nt_startdate <= '".date("Y-m-d")."' AND nt_enddate >= '".date("Y-m-d")."') 
						AND n.nt_id IN (SELECT nt_id FROM notification_apps WHERE apk_id=".$apk_id.") AND n.nt_created_by='".$params['userID']."'";
					}
					else{
						$strQuery1 = "SELECT n.*, b.msb_id, z.nzones_id, z.nzones_name, t.tpl_name FROM notifications AS n 
						INNER JOIN notification_beacons AS b ON b.nt_id=n.nt_id AND b.msb_id=".$row->msb_id." 
						LEFT OUTER JOIN templates AS t ON t.tpl_id=n.tpl_id 
						LEFT OUTER JOIN notification_zones AS z ON z.nzones_id=n.nzone_id
						WHERE n.status_id=1 ".$catQuery." AND (nt_startdate <= '".date("Y-m-d")."' AND nt_enddate >= '".date("Y-m-d")."') 
						AND n.nt_id IN (SELECT nt_id FROM notification_apps WHERE apk_id=".$apk_id.")";
					}
					//print("<br><br>".$strQuery1);
					$rs1 = mysql_query($strQuery1) or die(mysql_error()); // die(array('status' => 0, 'message' => mysql_error()));
					if(mysql_num_rows($rs1)>0){
						while($row1 = mysql_fetch_object($rs1)){
							//$nt_url = "";
							//if($row1->tpl_id>0){
								$nt_url = "http://beaconwatcher.com/api/template.php?nt_id=".$row1->nt_id;
							//}
							//$retValue['data']['notifications'][] = array('nt_id' => $row1->nt_id, 'nt_name' => $row1->nt_name, 'nt_title' => $row1->nt_title, 'nt_details' => $row1->nt_details, 'zone' => $row1->nzones_id, 'template' => $row1->tpl_name);
							//$noti[] = array('nt_id' => $row1->nt_id, 'nt_name' => $row1->nt_name, 'nt_title' => $row1->nt_title, 'nt_details' => $row1->nt_details, 'zone' => $row1->nzones_id, 'template' => $row1->tpl_name);
							$noti[] = array('nt_id' => $row1->nt_id, 'nt_name' => $row1->nt_name, 'nt_title' => $row1->nt_title, 'nt_details' => $row1->nt_details, 'zone' => $row1->nzones_id, 'template' => $nt_url);
						}
					}
					/*else{
						$noti = array();
					}*/
					$retValue['data'][] = array('msb_id' => $row->msb_id, 'msb_name' => $row->msb_name, 'msb_location' => $row->msb_location, 'msb_uuid' => $row->msb_uuid, 'msb_minor' => $row->msb_minor, 'msb_major' => $row->msb_major, 'msb_txpower' => $row->msb_txpower, 'msb_mac_address' => $row->msb_mac_address, 'site_id' => $row->site_id, 'site_title' => $row->site_title, 'company' => $row->mem_company, 'notifications' => $noti);
				}
			}
			else{
				$retValue = array('status' => 0, 'message' => 'No record found');
			}
		}
		//$retValue = array('status' => 0, 'message' => $strQuery);
		//print($strQuery);
		return $retValue;
    }
}