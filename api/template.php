<?php
include("../lib/openCon.php");

$defaultHTML = '<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Clip2net</title>
<meta name="viewport" content="width=device-width" />
<style type="text/css">
body{background-color: [!!BG_COLOR!!]; color:[!!FONT_COLOR!!];}
.container{width: 100%; float: left; padding: 0 0 15px 0;}
.parag{text-align: center;}
h1{text-align:center; margin:4% 0; color:[!!FONT_COLOR!!];}
p{font-family:"Times New Roman", Times, serif; font-size:18px; margin:6px 5%; min-height:100px; color:[!!FONT_COLOR!!]}

.img_rp{width:90%; float:left; text-align:center; margin:0 10%; height:300px; background-position:top; background-size:contain; background-repeat:no-repeat;}
.img_rp  img {/*width: 90%;*/}
footer{width:100%; text-align:center; position:absolute; bottom:6px; text-align:center;}

@media only screen and (max-device-width: 800px) and (max-device-height:1280px) {
	.heading{ padding: 30px 0;}
	.img_rp{height:644px;}
	.img_rp img { width:74%;}
}

@media only screen and (max-device-width: 768px) and (max-device-height:1024px) {
	p{min-height:100px;}
	.heading{padding:15px 0;}
	.img_rp{height:300px;}
	.img_rp img { width:60%;}
}

@media only screen and (max-device-width: 320px) and (max-device-height: 480px) {
	p{min-height:80px;}
	.img_rp{height:200px;}
	.img_rp img { width:60%;}
}
</style>
</head>

<body>
<div class="container">
	<h1>[!!HEADING!!]</h1>
	<p style="text-align:center;">[!!CONTENT!!]</p>
	<div class="img_rp" style="[!!IMAGE!!]"></div>
	<div class="clear_fix"></div>
	<footer>
		<div style="width:auto; margin:auto;" align="center">
			<a href="#"><img src="http://www.beaconwatcher.com/templates/1/img/img.png" /></a>
			<a href="#"><img src="http://www.beaconwatcher.com/templates/1/img/twt.png" /></a>
			<a href="#"><img src="http://www.beaconwatcher.com/templates/1/img/pint.png" /></a>
			<a href="#"><img src="http://www.beaconwatcher.com/templates/1/img/g_plus.png" /></a>
		</div>
	</footer>
</div>
</body>
</html>
';


$nBG = '#85cdc2';
$nFontColor = '#000000';
//$nImage = '<img src="http://www.beaconwatcher.com/templates/1/img/tea.png" alt="tea" />';
$nImage = "background-image:url('http://www.beaconwatcher.com/templates/1/img/tea.png');";
$imgPath = 'http://www.beaconwatcher.com/manager/files/notifications/';
$tmplHTML = '';
if(isset($_REQUEST['nt_id'])){
	$rs = mysql_query("SELECT n.*, t.tpl_code FROM notifications AS n LEFT OUTER JOIN templates AS t ON t.tpl_id=n.tpl_id WHERE n.nt_id=".$_REQUEST['nt_id']);
	if(mysql_num_rows($rs)>0){
		$row = mysql_fetch_object($rs);
		if(!empty($row->nt_bgcolor)){
			$nBG = $row->nt_bgcolor;
		}
		if(!empty($row->nt_fontcolor)){
			$nFontColor = $row->nt_fontcolor;
		}
		if(!empty($row->nt_image)){
			//$nImage = '<img src="'.$imgPath.$row->nt_image.'" alt="" />';
			$nImage = "background-image:url('".$imgPath.$row->nt_image."');";
		}
		/*else{
			$nImage = "";
		}*/
		if($row->tpl_id>0){
			$tmplHTML = $row->tpl_code;
		}
		else{
			$tmplHTML = $defaultHTML;
		}
		$tmplHTML = str_replace('[!!BG_COLOR!!]', $nBG, $tmplHTML);
		$tmplHTML = str_replace('[!!FONT_COLOR!!]', $nFontColor, $tmplHTML);
		$tmplHTML = str_replace('[!!IMAGE!!]', $nImage, $tmplHTML);
		$tmplHTML = str_replace('[!!HEADING!!]', $row->nt_title, $tmplHTML);
		$tmplHTML = str_replace('[!!CONTENT!!]', $row->nt_details, $tmplHTML);
		$tmplHTML = str_replace('[!!FACEBOOK_URL!!]', $row->nt_url_fb, $tmplHTML);
		$tmplHTML = str_replace('[!!TWITTER_URL!!]', $row->nt_url_tw, $tmplHTML);
		$tmplHTML = str_replace('[!!GOOGLEPLUS_URL!!]', $row->nt_url_gp, $tmplHTML);
		$tmplHTML = str_replace('[!!PINTREST_URL!!]', $row->nt_url_pn, $tmplHTML);
		print($tmplHTML);
	}
	else{
		print("Notification template not found!");
	}
}
?>