<?php
ob_start();
include("../../lib/openCon.php");
session_start();
//print(md5("admin"));
$strMSG="";
if (isset($_REQUEST['btnLogin'])){
	$rs = mysql_query("SELECT * FROM members WHERE mem_password='".md5($_REQUEST['mem_password'])."' AND mem_login='".$_REQUEST['mem_login']."'") or die(mysql_error());
	if(mysql_num_rows($rs)>0){
		$row = mysql_fetch_object($rs);
		if($row->mem_isadmin == 1){
			$_SESSION["isAdmin"] = 1;
		}
		else{
			$_SESSION["isAdmin"] = 0;
		}
			$_SESSION["UserID"] = $row->mem_id;
			$_SESSION["mainUserID"] = $row->mem_id;
			$_SESSION["UserName"] = $row->mem_login;
			$_SESSION["UType"] = $row->utype_id;
			$_SESSION['member_id'] = 0;
			$_SESSION['my_apk_id'] = 0;
			$_SESSION['appName'] = "";
			$_SESSION['appKey'] = "";
			if($_SESSION["UType"] > 2){
				$_SESSION['member_id'] = $row->mem_id;
				if($_SESSION["UType"]==4){
					$_SESSION['my_apk_id'] = $row->apk_id;
					$rs1 = mysql_query("SELECT * FROM mem_appkey WHERE apk_id=".$row->apk_id);
					if(mysql_num_rows($rs1)>0){
						$row1 = mysql_fetch_object($rs1);
						$_SESSION['appName'] = $row1->apk_title;
						$_SESSION['appKey'] = $row1->apk_key;
						$_SESSION["mainUserID"] = $row1->mem_id;
					}
				}
			}
			/*else{
				$_SESSION['member_id'] = 0;
			}*/
			/*if($_SESSION["UType"] == 3){
				$_SESSION['member_id'] = $row->mem_id;
			}
			else{
				$_SESSION['member_id'] = 0;
			}*/
			$_SESSION["FName"] = $row->mem_fname;
			$_SESSION["LName"] = $row->mem_lname;
		header("location: ../index.php");
	}
	else{
		$strMSG = '<div class="alert alert-danger" style="width:90%; margin-left:5%;">Invalid Login / Password</div>';
	}
}
if(isset($_REQUEST['op'])){
	switch($_REQUEST['op']){
		case 1:
			$strMSG = '<div class="alert alert-success" style="width:90%; margin-left:5%;">Registered Successfully!</div>';
		break;
	}
}
ob_end_flush();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Manager :: BeaconWatcher</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Loading Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">

  <!-- Loading Stylesheets -->    
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">
  <link href="css/login.css" rel="stylesheet">
</head>
<body >
  <div class="login-box">
  	<div align="center"><img src="../images/logo2_BW.png" width="150" /></div>
	<div align="center"><img src="../images/logo_beaconswatcher.png" width="240" /></div>
    <!--<h1 style="padding-top:0px; margin-top:0px;"><i class='fa fa-bookmark'></i>&nbsp;BeaconWatcher </h1>-->
    <hr>
    <h5>LOGIN</h5>
	<?php //print(md5("admin"));?>
	<?php print($strMSG); ?>
    <div class="input-box">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
          <form role="form" method="post" action="<?php print($_SERVER['PHP_SELF']);?>">
            <div class="input-group form-group">
              <span class="input-group-addon"><i class='fa fa-envelope'></i></span>
              <input type="Email" class="form-control" placeholder="Email" name="mem_login">
            </div>
            <div class="input-group form-group">
              <span class="input-group-addon"><i class='fa fa-key'></i></span>
              <input type="Password" class="form-control" placeholder="Password" name="mem_password">
            </div>
            <div class="form-group">
              <button type="submit" name="btnLogin" class="btn  btn-block  btn-submit pull-right" style="margin-bottom:10px;">Submit</button>
            </div>
		  </form>
        </div>
		<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1" style="margin-bottom:18px;">
			<a href="register.php">New user Sign Up here</a>
		</div>
        <!-- col -->
      </div>
      <!-- row -->
    </div>
    <!-- input-box -->
  </div>
  <!-- lock-box -->
</body>
</html>