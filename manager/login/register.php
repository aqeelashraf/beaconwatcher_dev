<?php
ob_start();
include("../../lib/openCon.php");
include("../../lib/functions.php");
session_start();
//print(md5("admin"));
$strMSG="";
if(isset($_REQUEST['btnAdd'])){
	$mem_id = 0;
	$mem_login = $_REQUEST['mem_login'];
	$mem_fname = $_REQUEST['mem_fname'];
	$mem_lname = $_REQUEST['mem_lname'];
	$mem_phone = $_REQUEST['mem_phone'];
	if(IsExist("mem_id", "members", "mem_login", $_REQUEST['mem_login'])){
		$strMSG = '<div class="alert alert-danger" style="width:90%; margin-left:5%;">Username already exist</div>';
	}
	else{
		$memid = getMaximum("members","mem_id");
		//print("INSERT INTO members(mem_id, mem_login, mem_password, mem_fname, mem_lname, mem_phone, mem_datecreated, mem_confirm, status_id, mem_isadmin, utype_id, apk_id) VALUES(".$memid.", '".$_REQUEST['mem_login']."', '".md5($_REQUEST['mem_password'])."', '".$_REQUEST['mem_fname']."', '".$_REQUEST['mem_lname']."', '".$_REQUEST['mem_phone']."', '".date("Y-m-d")."', '1', '1', '0', '3', '0')");
		mysql_query("INSERT INTO members(mem_id, mem_login, mem_password, mem_fname, mem_lname, mem_phone, mem_datecreated, mem_confirm, status_id, mem_isadmin, utype_id, apk_id) VALUES(".$memid.", '".$_REQUEST['mem_login']."', '".md5($_REQUEST['mem_password'])."', '".$_REQUEST['mem_fname']."', '".$_REQUEST['mem_lname']."', '".$_REQUEST['mem_phone']."', '".date("Y-m-d")."', '1', '1', '0', '3', '0')") or die(mysql_error());
		//header("Location: ".$_SERVER['PHP_SELF']."?".$qryStrURL."op=1");
		header("Location: login.php?op=1");
	}
}
else{
	$mem_id = "";
	$mem_login = "";
	$mem_fname = "";
	$mem_lname = "";
	$mem_phone = "";
}
ob_end_flush();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Manager :: BeaconWatcher</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Loading Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">

  <!-- Loading Stylesheets -->    
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">
  <link href="css/reg.css" rel="stylesheet">
  

</head>
<body >
 <div class="reg-box">
 	<div align="center"><img src="../images/logo2_BW.png" width="150" /></div>
	<div align="center"><img src="../images/logo_beaconswatcher.png" width="240" /></div>
    <!--<h1><i class='fa fa-bookmark'></i>&nbsp;Welcome To Cascade </h1>-->
    <hr>
  
	<h5>REGISTER</h5>
	<?php print($strMSG); ?>
    <div class="reg-submit-box">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
          <form name="frm" method="post" role="form" action="<?php print($_SERVER['PHP_SELF']);?>">
            <div class="input-group form-group">
              <span class="input-group-addon"><i class='fa fa-envelope'></i></span>
              <input type="email" class="form-control" name="mem_login" id="mem_login" value="<?php print($mem_login);?>" placeholder="E-mail">
            </div>
            <div class="input-group form-group">
              <span class="input-group-addon"><i class='fa fa-key'></i></span>
              <input type="password" class="form-control" name="mem_password" id="mem_password" placeholder="Password">
            </div>
			<div class="input-group form-group">
              <span class="input-group-addon"><i class='fa fa-user'></i></span>
              <input type="text" class="form-control" name="mem_fname" id="mem_fname" value="<?php print($mem_fname);?>" placeholder="First Name">
            </div>
			<div class="input-group form-group">
              <span class="input-group-addon"><i class='fa fa-user'></i></span>
              <input type="text" class="form-control" name="mem_lname" id="mem_lname" value="<?php print($mem_lname);?>" placeholder="Last Name">
            </div>
			<div class="input-group form-group">
              <span class="input-group-addon"><i class='fa fa-phone'></i></span>
              <input type="tel" class="form-control" name="mem_phone" id="mem_phone" value="<?php print($mem_phone);?>" placeholder="Phone">
            </div>
			<!-- Button trigger modal -->
            <h6><input type="checkbox"><a href="#" data-toggle="modal" data-target="#myModal">I agree to terms &amp; Services</a></h6>
          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i> </button>
                  <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                </div>
                <div class="modal-body">  
                  <ul>
                  <h4>Welcome to Cascade!</h4>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero, laborum, iusto, facilis, consequatur quis culpa consequuntur animi excepturi vitae eaque molestiae amet ad. Debitis, saepe eveniet earum qui recusandae explicabo!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, expedita magnam laboriosam recusandae ut eaque doloribus. Ex, aut, in, illo, quia tempore repudiandae dignissimos nostrum non consequatur nesciunt tenetur corporis.</li>
                    <h4>Modifying and Terminating our Services</h4>
                    
                      <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, deleniti, temporibus, voluptas commodi sint accusantium soluta eaque possimus amet eius culpa dolore accusamus omnis rerum esse tenetur ea ex cupiditate.</li>
                      <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate, neque, quaerat, exercitationem voluptatum illo sunt voluptatem inventore tempore nemo molestias est at temporibus repellendus incidunt pariatur voluptates nesciunt illum vel!</li>              
                  </ul>
                </div>
                <div class="modal-footer">
                  
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->


         
            <div class="form-group" style="margin-bottom:30px; padding-bottom:20px;">
              <button type="submit" name="btnAdd" class="btn  btn-block  btn-submit pull-right">Register</button>
            </div>
            
          </form>
        </div>
        <!-- col -->
      </div>
      <!-- mail-box -->

    </div>
    <!-- reg-box -->

  </div>
  <!-- row -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>