<?php include('includes/header.php'); ?>
			<!-- /header -->
			<div class="row">
				<div class="col-mod-12">
					<ul class="breadcrumb">
						<li><a href="index.php">Dashboard</a></li>
						<!--<li><a href="template.php">Basic Template</a></li>
						<li class="active">BreadCrumb</li>-->
					</ul>
					<!--<div class="form-group hiddn-minibar pull-right">
						<input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />
						<span class="input-icon fui-search"></span> </div>-->
					<h3 class="page-header"> Dashboard <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
					<blockquote class="page-information hidden">
						<p> <b>Dashboard</b> is the basic page where you can add sections according to your requirements easily within this division. </p>
					</blockquote>
				</div>
			</div>
			
			
			<!-- Info Boxes -->
			<div class="row">
				<div class="col-md-4">
					<div class="info-box  bg-info  text-white">
						<div class="info-icon bg-info-dark"> <i class="fa fa-mobile fa-4x"></i> </div>
						<div class="info-details">
							<h4>Apps <span class="pull-right"><?php print(TotalRecords("mem_appkey", ""));?></span></h4>
							<p>Total <span class="badge pull-right bg-white text-info"> <!--48%--> <i class="fa fa-arrow-up fa-1x"></i><!--<i class="fa fa-arrow-down fa-1x"></i>--> </span> </p>
						</div>
					</div>
				</div>
				<div class="col-md-4 ">
					<div class="info-box  bg-success  text-white"  id="initial-tour">
						<div class="info-icon bg-success-dark"> <i class="fa fa-home fa-4x"></i> </div>
						<div class="info-details">
							<h4>Venues <span class="pull-right"><?php print(TotalRecords("mem_sites", ""));?></span></h4>
							<p>Total <span class="badge pull-right bg-white text-success"> <!--98%--> <i class="fa fa-arrow-up fa-1x"></i> </span> </p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-box  bg-warning  text-white">
						<div class="info-icon bg-warning-dark"> <i class="fa fa-comments fa-4x"></i> </div>
						<div class="info-details">
							<h4>Beacons <span class="pull-right"><?php print(TotalRecords("msite_beacons", ""));?></span></h4>
							<p>Total <span class="badge pull-right bg-white text-warning"> <!--78%--> <i class="fa fa-arrow-up fa-1x"></i> </span> </p>
						</div>
					</div>
				</div>
			</div>
			<div class="row hidden">
				<div class="col-md-4">
					<div class="info-box  bg-primary  text-white">
						<div class="info-icon bg-primary-dark"> <i class="fa fa-refresh fa-4x"></i> </div>
						<div class="info-details">
							<h4>Visits <span class="pull-right">2,43,333</span></h4>
							<p>Today <span class="badge pull-right bg-white text-primary"> 48% <i class="fa fa-arrow-down fa-1x"></i> </span> </p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-box  bg-danger  text-white">
						<div class="info-icon bg-danger-dark"> <i class="fa fa-rotate-left fa-4x"></i> </div>
						<div class="info-details">
							<h4>Returns <span class="pull-right">33</span></h4>
							<p>This Year <span class="badge pull-right bg-white text-danger"> 28% <i class="fa fa-arrow-down fa-1x"></i> </span> </p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-box  bg-info  text-white">
						<div class="info-icon bg-primary-dark"> <i class="fa fa-dollar fa-4x"></i> </div>
						<div class="info-details">
							<h4>Profit <span class="pull-right">$98,233</span></h4>
							<p>Projected <span class="badge pull-right bg-success text-white"> 48% <i class="fa fa-arrow-up fa-1x"></i> </span> </p>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Demo Panel -->
<!--			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-cascade">
						<div class="panel-heading">
							<h3 class="panel-title text-primary"> Demo Panel <span class="pull-right"> <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a> <a href="#" class="panel-close"><i class="fa fa-times"></i></a> </span> </h3>
						</div>
						<div class="panel-body panel-border"> This is a basic template page to quick start your project. </div>
					</div>
				</div>
			</div>-->
		</div>
		<!-- /.content --> 
		
		<!-- .right-sidebar -->
		<?php include("includes/rightsidebar.php")?>
	</div>
	<!-- /.right-sidebar --> 
	
	<!-- /rightside bar --> 
	
</div>
<!-- /.box-holder -->
</div>
<!-- /.site-holder -->

<?php include("includes/bottom_js.php")?>
</body>
</html>
<?php include("../lib/closeCon.php"); ?>