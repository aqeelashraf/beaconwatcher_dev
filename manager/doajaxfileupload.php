<?php
	$error = "";
	$msg = "";
	$fileElementName = 'iFile';
	/*if(!empty($_FILES[$fileElementName]['error'])){
		switch($_FILES[$fileElementName]['error']){*/
	if(!empty($_FILES['iFile']['error'])){
		switch($_FILES['iFile']['error']){
			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;
			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}
	elseif(empty($_FILES['iFile']['tmp_name']) || $_FILES['iFile']['tmp_name'] == 'none'){
		$error = 'No file was uploaded..';
	}
	else{
		$dateTime = date("Ymd_his");
		$filePath = "files/tmp/".$dateTime."_".$_FILES['iFile']['name'];
		if(move_uploaded_file($_FILES['iFile']['tmp_name'], $filePath)){
			$msg .= $filePath;
			//for security reason, we force to remove all uploaded file
			@unlink($_FILES['iFile']);
		}
		else{
			$error = 'File not uploaded. Please try later';
		}
	}		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
?>