$(function() {
    // Code for those demos
    var _createColorpickers = function() {
        $('#cp1').colorpicker({
            format: 'hex'
        });
        $('#nt_bgcolor').colorpicker();
        $('#nt_fontcolor').colorpicker();
		$('#tpl_bg_color').colorpicker();
        $('#tpl_font_color').colorpicker();
        /*$('#cp4').colorpicker();*/
    }

    _createColorpickers();

    $('.bscp-destroy').click(function(e) {
        e.preventDefault();
        $('.bscp').colorpicker('destroy');
    });

    $('.bscp-create').click(function(e) {
        e.preventDefault();
        _createColorpickers();
    });


});
