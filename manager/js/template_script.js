$(document).ready(function(e) {
	$(".tplOptions").hide();
	$("#tpl_id").change(function(e) {
		$('#overlay').show();
		var postData = $(this).serializeArray();
		var formURL = "ajax.php?action=getTemplateHTML";
		$.ajax({
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR){
				//alert(data);
				var result = data.split("||");
				$("#preview").html(result[2]);
				$("#nt_bgcolor").val(result[0]);
				$("#nt_fontcolor").val(result[1]);
				//$("#preview").html(data);
				$('#overlay').hide();
				if ($("#nt_title").length > 0) {
					$("#preview").find("h1").html($("#nt_title").val());
				}
				if ($("#nt_details").length > 0) {
					$("#preview").find("p").html($("#nt_details").val());
				}
				if ($("#nt_bgcolor").length > 0) {
					$("#preview").css("background-color", $("#nt_bgcolor").val());
				}
				if ($("#nt_fontcolor").length > 0) {
					$("#preview").css("color", $("#nt_fontcolor").val());
				}
				if ($("#nt_image").length > 0) {
					$("#preview").find("div.img_rp").css("background-image", "url('files/notifications/"+$("#nt_image").val()+"')");
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				//if fails      
				alert("Error");
			}
		});
		if($(this).val()>0){
			$(".tplOptions").show('slow');
		}
		else{
			$(".tplOptions").hide();
		}
	});
	
	$("#nt_bgcolor").blur(function(e) {
		$("#preview").css("background-color", $("#nt_bgcolor").val());
	});
	$("#nt_fontcolor").blur(function(e) {
		$("#preview").css("color", $("#nt_fontcolor").val());
	});
	$("#nt_title").keyup(function(e) {
		$("#preview").find("h1").html($("#nt_title").val());
	});
	$("#nt_details").keyup(function(e) {
		$("#preview").find("p").html($("#nt_details").val());
	});
	
	$(document).on('change','#iFile' , function(){
		$('#overlay').show();
		ajaxFileUpload();
		$('#overlay').hide();
	});
	
	function ajaxFileUpload(){
        $.ajaxFileUpload({
			url:'doajaxfileupload.php',
			secureuri:false,
			fileElementId:'iFile',
			dataType: 'json',
			success: function (data, status){
				if(typeof(data.error) != 'undefined'){
					if(data.error != ''){
						alert(data.error);
					}
					else{
						//alert(data.msg);
						//$("div.img_rp").html('<img src="'+data.msg+'">');
						$("div.img_rp").css("background-image", "url('"+data.msg+"')");
					}
				}
			},
			error: function (data, status, e){
				alert(e);
			}
		})
        return false;
    }  
});