<?php include('includes/header.php'); ?>
			<!-- /header -->
			<div class="row">
				<div class="col-mod-12">
					<ul class="breadcrumb">
						<li><a href="index.php">Dashboard</a></li>
						<li><a href="template.php">Basic Template</a></li>
						<li class="active">BreadCrumb</li>
					</ul>
					<div class="form-group hiddn-minibar pull-right">
						<input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />
						<span class="input-icon fui-search"></span> </div>
					<h3 class="page-header"> Basic Template <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
					<blockquote class="page-information hidden">
						<p> <b>Template Page</b> is the basic page where you can add more pages according to your requirements easily within this division. </p>
					</blockquote>
				</div>
			</div>
			
			<!-- Demo Panel -->
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-cascade">
						<div class="panel-heading">
							<h3 class="panel-title text-primary"> Demo Panel <span class="pull-right"> <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a> <a href="#" class="panel-close"><i class="fa fa-times"></i></a> </span> </h3>
						</div>
						<div class="panel-body panel-border"> This is a basic template page to quick start your project. </div>
						<!-- /panel body --> 
					</div>
				</div>
			</div>
		</div>
		<!-- /.content --> 
		
		<!-- .right-sidebar -->
		<?php include("includes/rightsidebar.php")?>
	</div>
	<!-- /.right-sidebar --> 
	
	<!-- /rightside bar --> 
	
</div>
<!-- /.box-holder -->
</div>
<!-- /.site-holder -->

<?php include("includes/bottom_js.php")?>
</body>
</html>