			<li class=' ' ><a href='index.php' data-original-title='Dashboard'><i class='fa fa-dashboard'></i><span class='hidden-minibar'> Dashboard</span></a></li> <li class='submenu  '><a class='dropdown' href='#' data-original-title='Management'><i class='fa fa-indent'></i><span class='hidden-minibar'> Management  <span class='badge bg-primary pull-right'>2</span></span></a><ul><li class='active' ><a href='manage_apps.php' data-original-title='My Apps'><i class='fa fa-gears'></i><span class='hidden-minibar'> My Apps</span></a></li><li class=' ' ><a href='manage_sites.php' data-original-title='My Sites'><i class='fa fa-gears'></i><span class='hidden-minibar'> My Sites</span></a></li></ul></li>


<?php
function buildMenu($menuList)
{
  // Get the link name
  $pieces = explode('/',$_SERVER['REQUEST_URI']);  
  $page=end($pieces); 

    foreach ($menuList as $val=>$node)
    {
      $active=(strpos($page,$node['link']) !== false) ? "active" : " ";  

        //Running array for Main parent links
        if (! empty($node['children'])) 
        {
          echo " <li class='submenu ". $active ."'><a class='dropdown' href='" . $node['link']. "' data-original-title='" . $node['title'] . "'><i class='fa fa-".$node['icon']."'></i><span class='hidden-minibar'> " . $node['title'] . "  <span class='badge bg-primary pull-right'>".count($node['children'])."</span></span></a>";
        }
        else
        {
          echo "<li class='". $active ."' ><a href='" . $node['link']. "' data-original-title='" . $node['title'] . "'><i class='fa fa-".$node['icon']."'></i><span class='hidden-minibar'> " . $node['title'] . "</span></a>";
        }
        

        // Running submenu
        if ( ! empty($node['children'])) 
        {
            echo "<ul>";
            buildMenu($node['children']);
            echo "</ul>";
        }
        echo "</li>";
    }
}

if($_SESSION["UType"]==3){
	$menuList = Array(
    0 => Array(
        'title' => 'Dashboard',
        'link' => 'index.php',
        'icon' => 'dashboard',
        'children' => Array()
    ),
    1 => Array(
        'title' => 'Management',
        'link' => '#',
        'icon' => 'indent',
        'children' => Array(
            0 => Array(
                'title' => 'My Apps',
                'link' => 'manage_apps.php',
                'icon' => 'gears',
                'children' => Array()
            ),
			1 => Array(
                'title' => 'My Sites',
                'link' => 'manage_sites.php',
                'icon' => 'gears',
                'children' => Array()
            )
		)
            
    )
);
}
else{
	$menuList = Array(
    0 => Array(
        'title' => 'Dashboard',
        'link' => 'index.php',
        'icon' => 'dashboard',
        'children' => Array()
    ),
    1 => Array(
        'title' => 'Management',
        'link' => '#',
        'icon' => 'indent',
        'children' => Array(
			0 => Array(
                'title' => 'Users',
                'link' => 'manage_users.php',
                'icon' => 'user',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Apps',
                'link' => 'manage_apps.php',
                'icon' => 'gears',
                'children' => Array()
            ),
			2 => Array(
                'title' => 'Sites',
                'link' => 'manage_sites.php',
                'icon' => 'gears',
                'children' => Array()
            )/*,
			2 => Array(
                'title' => 'Beacons',
                'link' => 'manage_beacons.php',
                'icon' => 'eye',
                'children' => Array()
            )*/
		)
            
    )
);
}

/*
$menuList = Array(
    0 => Array(
        'title' => 'Dashboard',
        'link' => 'index.php',
        'icon' => 'dashboard',
        'children' => Array()
    ),
    1 => Array(
        'title' => 'Management',
        'link' => '#',
        'icon' => 'indent',
        'children' => Array(
			0 => Array(
                'title' => 'Users',
                'link' => 'manage_users.php',
                'icon' => 'user',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Apps',
                'link' => 'manage_apps.php',
                'icon' => 'gears',
                'children' => Array()
            ),
			2 => Array(
                'title' => 'Sites',
                'link' => 'manage_sites.php',
                'icon' => 'gears',
                'children' => Array()
            )
		)
            
    ),
    2 => Array(
        'title' => 'Pages',
        'link' => '#',
        'icon' => 'book',
        'children' => Array(
            0 => Array(
                'title' => 'Calendar',
                'link' => 'calendar.php',
                'icon' => 'calendar',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Chat',
                'link' => 'chat.php',
                'icon' => 'comment',
                'children' => Array()
            ),
            2 => Array(
                'title' => 'Profile',
                'link' => 'profile.php',
                'icon' => 'user',
                'children' => Array()
            ),
            3 => Array(
                'title' => 'Gallery',
                'link' => 'gallery.php',
                'icon' => 'th',
                'children' => Array()
            ),
            4 => Array(
                'title' => 'Grid',
                'link' => 'grids.php',
                'icon' => 'th-large',
                'children' => Array()
            ),
            5 => Array(
                'title' => 'Images',
                'link' => 'images.php',
                'icon' => 'picture-o',
                'children' => Array()
            ),
            6 => Array(
                'title' => 'Inbox',
                'link' => 'inbox.php',
                'icon' => 'envelope',
                'children' => Array()
            ),
            7 => Array(
                'title' => 'Invoice',
                'link' => 'invoice.php',
                'icon' => 'credit-card',
                'children' => Array()
            ),
            8 => Array(
                'title' => 'Pricing',
                'link' => 'pricing-table.php',
                'icon' => 'money',
                'children' => Array()
            ),
            9 => Array(
                'title' => 'Support',
                'link' => 'support.php',
                'icon' => 'gears',
                'children' => Array()
            ),
            10 => Array(
                'title' => 'Typography',
                'link' => 'typography.php',
                'icon' => 'text-width',
                'children' => Array()
            )
        )
    ),
    3 => Array(
        'title' => 'Utility',
        'link' => '#',
        'icon' => 'tint',
        'children' => Array(
            0 => Array(
                'title' => '404',
                'link' => '404.php',
                'icon' => 'exclamation-circle',
                'children' => Array(),
            ),
            1 => Array(
                'title' => '505',
                'link' => '505.php',
                'icon' => 'exclamation-circle',
                'children' => Array()
           ),
            2 => Array(
                'title' => 'FAQ',
                'link' => 'faq.php',
                'icon' => 'question',
                'children' => Array()
           ),
            3 => Array(
                'title' => 'Lock Screen',
                'link' => 'screens.php',
                'icon' => 'lock',
                'children' => Array()
           ),
            4 => Array(
                'title' => 'Signin',
                'link' => 'screens.php',
                'icon' => 'sign-in',
                'children' => Array()
           ),
            5 => Array(
                'title' => 'Sign Up',
                'link' => 'screens.php',
                'icon' => 'smile-o',
                'children' => Array()
           ),
            6 => Array(
                'title' => 'Template',
                'link' => 'template.php',
                'icon' => 'pagelines',
                'children' => Array()
           ),
        )
    ),
    4 => Array(
        'title' => 'UI Elements',
        'link' => '#',
        'icon' => 'user',
        'children' => Array(
            0 => Array(
                'title' => 'Alerts',
                'link' => 'alerts.php',
                'icon' => 'exclamation-triangle',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Animations',
                'link' => 'animations.php',
                'icon' => 'font',
                'children' => Array()
           ),
            2 => Array(
                'title' => 'BreadCrubms',
                'link' => 'breadcrumbs-jumbotron.php',
                'icon' => 'chain',
                'children' => Array()
           ),
            3 => Array(
                'title' => 'Buttons',
                'link' => 'buttons.php',
                'icon' => 'lock',
                'children' => Array()
           ),
            4 => Array(
                'title' => 'Carousel',
                'link' => 'carousel.php',
                'icon' => 'coffee',
                'children' => Array()
           ),
            5 => Array(
                'title' => 'Notifications',
                'link' => 'notifications.php',
                'icon' => 'bell-o',
                'children' => Array()
           ),
            6 => Array(
                'title' => 'Labels Badges',
                'link' => 'labels-badges.php',
                'icon' => 'phone-square',
                'children' => Array()
           ),
            7 => Array(
                'title' => 'List Groups',
                'link' => 'list-groups.php',
                'icon' => 'dot-circle-o',
                'children' => Array()
           ),
            8 => Array(
                'title' => 'Pagination',
                'link' => 'pagination.php',
                'icon' => 'sort-numeric-asc',
                'children' => Array()
           ),
            9 => Array(
                'title' => 'Panels',
                'link' => 'panels.php',
                'icon' => 'windows',
                'children' => Array()
           ),
            10 => Array(
                'title' => 'Progress Bars',
                'link' => 'progress-bars.php',
                'icon' => 'ruble',
                'children' => Array()
           ),
            11 => Array(
                'title' => 'Sliders',
                'link' => 'sliders.php',
                'icon' => 'exchange',
                'children' => Array()
           ),
            12 => Array(
                'title' => 'Tabs Accordians',
                'link' => 'tabs-accordians.php',
                'icon' => 'check',
                'children' => Array()
           ),
            13 => Array(
                'title' => 'Info Boxes',
                'link' => 'info-boxes.php',
                'icon' => 'bullseye',
                'children' => Array()
           ),
            14 => Array(
                'title' => 'Tooltips Popovers',
                'link' => 'tooltips-popovers.php',
                'icon' => 'asterisk',
                'children' => Array()
           ),
            15 => Array(
                'title' => 'Wells',
                'link' => 'wells.php',
                'icon' => 'beer',
                'children' => Array()
           ),
            16 => Array(
                'title' => 'Draggable Portlets',
                'link' => 'draggable-portlets.php',
                'icon' => 'windows',
                'children' => Array()
           ),
            17 => Array(
                'title' => 'Bootbox Modals',
                'link' => 'bootbox-modals.php',
                'icon' => 'windows',
                'children' => Array()
           ),
            18 => Array(
                'title' => 'Extended Modals',
                'link' => 'extended-modals.php',
                'icon' => 'windows',
                'children' => Array()
           ),
            19 => Array(
                'title' => 'Date Paginator',
                'link' => 'date-paginator.php',
                'icon' => 'windows',
                'children' => Array()
           ),
            20 => Array(
                'title' => 'Tree View',
                'link' => 'treeview.php',
                'icon' => 'windows',
                'children' => Array()
           ),
        )
    ),
   5 => Array(
        'title' => 'Tables',
        'link' => '#',
        'icon' => 'table',
        'children' => Array(
            0 => Array(
                'title' => 'Basic Tables',
                'link' => 'basic-tables.php',
                'icon' => 'table',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Editable Tables',
                'link' => 'editable-tables.php',
                'icon' => 'table',
                'children' => Array(),
            ),
            2 => Array(
                'title' => 'Dynamic Tables',
                'link' => 'dynamic-tables.php',
                'icon' => 'table',
                'children' => Array(),
            ),
        )
    ),
   6 => Array(
        'title' => 'Unlimited Menu',
        'link' => '#',
        'icon' => 'sitemap',
        'children' => Array(
            0 => Array(
                'title' => 'Submenu',
                'link' => '#',
                'icon' => 'android',
                'children' => Array(),
            ),
            2 => Array(
                'title' => 'Submenu',
                'link' => '#',
                'icon' => 'apple',
                'children' => Array(),
            ),
            3 => Array(
                'title' => 'One More',
                'link' => '#',
                'icon' => 'android',
                'children' => Array(
                    0 => Array(
                        'title' => 'Submenu',
                        'link' => '#',
                        'icon' => 'android',
                        'children' => Array(),
                    ),
                    2 => Array(
                        'title' => 'Submenu',
                        'link' => '#',
                        'icon' => 'apple',
                        'children' => Array(),
                    ),
                    3 => Array(
                        'title' => 'One More',
                        'link' => '#',
                        'icon' => 'android',
                        'children' => Array(
                            0 => Array(
                                'title' => 'Submenu',
                                'link' => '#',
                                'icon' => 'android',
                                'children' => Array(),
                            ),
                            2 => Array(
                                'title' => 'Submenu',
                                'link' => '#',
                                'icon' => 'apple',
                                'children' => Array(),
                            ),
                            3 => Array(
                              'title' => 'Trust Me &amp; More',
                              'link' => '#',
                              'icon' => 'android',
                              'children' => Array(),
                            )
                        ),
                    )
                ),
            ),
        )
    ),
   7 => Array(
        'title' => 'Forms',
        'link' => '#',
        'icon' => 'list-alt',
        'children' => Array(
            0 => Array(
                'title' => 'Dropzone File Upload',
                'link' => 'dropzone-file-upload.php',
                'icon' => 'level-down',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Form Input Masks',
                'link' => 'form-input-masks.php',
                'icon' => 'pencil-square',
                'children' => Array(),
            ),
            2 => Array(
                'title' => 'Form Validation',
                'link' => 'form-validation.php',
                'icon' => 'warning',
                'children' => Array(),
            ),
            3 => Array(
                'title' => 'Form Wizard',
                'link' => 'form-wizard.php',
                'icon' => 'indent',
                'children' => Array(),
            ),
            4 => Array(
                'title' => 'Input Groups',
                'link' => 'input-groups.php',
                'icon' => 'group',
                'children' => Array(),
            ),
            5 => Array(
                'title' => 'Layouts &nbsp; Elements',
                'link' => 'layouts-elements.php',
                'icon' => 'indent',
                'children' => Array(),
            ),
            6 => Array(
                'title' => 'Multiple File Upload',
                'link' => 'multiple-file-upload.php',
                'icon' => 'cloud-upload',
                'children' => Array(),
            ),
            7 => Array(
                'title' => 'Pickers',
                'link' => 'pickers.php',
                'icon' => 'eye',
                'children' => Array(),
            ),
            8 => Array(
                'title' => 'Wysiwyg &amp; Markdown',
                'link' => 'wysiwyg-markdown.php',
                'icon' => 'pencil-square',
                'children' => Array(),
            ),
        )
    ),
   8 => Array(
        'title' => 'Charts',
        'link' => '#',
        'icon' => 'bar-chart-o',
        'children' => Array(
            0 => Array(
                'title' => 'Basic Charts',
                'link' => 'basic-charts.php',
                'icon' => 'bar-chart-o',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Live Charts',
                'link' => 'live-charts.php',
                'icon' => 'bar-chart-o',
                'children' => Array(),
            ),
            2 => Array(
                'title' => 'Morris Charts',
                'link' => 'morris.php',
                'icon' => 'bar-chart-o',
                'children' => Array(),
            ),
            3 => Array(
                'title' => 'Pie Charts',
                'link' => 'pie-charts.php',
                'icon' => 'bar-chart-o',
                'children' => Array(),
            ),
            4 => Array(
                'title' => 'Spark Lines',
                'link' => 'sparklines.php',
                'icon' => 'bar-chart-o',
                'children' => Array(),
            ),
            5 => Array(
                'title' => 'NVD3 Charts',
                'link' => 'nvd3.php',
                'icon' => 'bar-chart-o',
                'children' => Array(),
            )
        )
    ),
   9 => Array(
        'title' => 'Maps',
        'link' => '#',
        'icon' => 'map-marker',
        'children' => Array(
            0 => Array(
                'title' => 'Google Maps',
                'link' => 'google-maps.php',
                'icon' => 'google-plus',
                'children' => Array(),
            ),
            1 => Array(
                'title' => 'Vector Maps',
                'link' => 'vector-maps.php',
                'icon' => 'vimeo-square',
                'children' => Array(),
            )
        )
    ),
   10 => Array(
        'title' => 'Icons',
        'link' => 'icons.php',
        'icon' => 'truck',
        'children' => Array()
    )
);*/
?>
