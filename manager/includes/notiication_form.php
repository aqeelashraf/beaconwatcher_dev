<div class="col-md-12">
	<div class="panel panel-dat">
		<div class="panel-heading">
			<h3 class="panel-title text-primary"> Notification  Wizard <!--<span class="pull-right"> <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>--> <!--<a href="#" class="panel-close"><i class="fa fa-times"></i></a>--> </span> </h3>
		</div>
		<div class="panel-body nopadding"> 
			
			<!-- Smart Wizard -->
			<form name="frm" id="frm" method="post" action="<?php print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form" enctype="multipart/form-data">
			<div id="wizard" class="swMain">
				<ul>
					<li><a href="#step-1">
						<label class="stepNumber">1</label>
						<span class="stepDesc"> General </span> </a></li>
					<li><a href="#step-2">
						<label class="stepNumber">2</label>
						<span class="stepDesc"> Triggers </span> </a></li>
					<li><a href="#step-3">
						<label class="stepNumber">3</label>
						<span class="stepDesc"> Audiance </span> </a></li>
					<li id="tmlpStep"><a href="#step-4">
						<label class="stepNumber">4</label>
						<span class="stepDesc"> Notification </span> </a></li>
				</ul>
				<div id="step-1">
					<h2 class="StepTitle">General</h2>
					<div class="panel">
						<!--<div class="panel-heading">
							<h3 class="panel-title"> Cascade Template </h3>
						</div>-->
						<div class="panel-body">
							<!--<form name="frm" id="frm" method="post" action="<?php //print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form">-->
							<div class="form-group">
								<label class="col-lg-2 col-md-3 control-label">Category</label>
								<div class="col-lg-10 col-md-9">
									<select data-placeholder="Select Category" class="chosen-select required" name="cat_id" id="cat_id" style="width:350px;" tabindex="2">
									<!--<select data-placeholder="Choose a Country..." class="chosen-select" style="width:350px;" tabindex="2">-->
										<!--<option value=""></option>-->
										<option value="0">N/A</option>
										<?php 
											FillSelected("categories", "cat_id", "cat_name", @$cat_id);
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="site_login" class="col-lg-2 col-md-3 control-label">Name:</label>
								<div class="col-lg-10 col-md-9">
									<input type="text" class="form-control form-cascade-control input_wid70 required" name="nt_name" id="nt_name" value="<?php @print($nt_name);?>" placeholder="Name">
								</div>
							</div>
							<div class="form-group">
								<label for="site_fname" class="col-lg-2 col-md-3 control-label">Start Date:</label>
								<div class="col-lg-10 col-md-9"> 
									<input type="text" class="form-control form-cascade-control input_wid70 datepicker" name="nt_startdate" id="nt_startdate" value="<?php @print($nt_startdate);?>" placeholder="Start Date">
								</div>
							</div>
							<div class="form-group">
								<label for="site_fname" class="col-lg-2 col-md-3 control-label">End Date:</label>
								<div class="col-lg-10 col-md-9"> 
									<input type="text" class="form-control form-cascade-control input_wid70 datepicker" name="nt_enddate" id="nt_enddate" value="<?php @print($nt_enddate);?>" placeholder="End Date">
								</div>
							</div>
							<!--</form>-->
						</div>
					</div>
				</div>
				<div id="step-2">
					<h2 class="StepTitle">Context Triggers</h2>
					<div class="panel-body" style="min-height:300px;">
					<!--<form class="form-horizontal" method="post" action="#" name="frm2" id="frm2">-->
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">App</label>
							<div class="col-lg-10 col-md-9">
							<?php 
								if($_SESSION['UType']>3){
									print('<label class="control-label">'.$_SESSION['appName'].' ('.$_SESSION['appKey'].')</label><input type="hidden" name="apk_id[]" id="apk_id" value="'.$_SESSION['my_apk_id'].'" />');
								} else{ 
							?>
								<select data-placeholder="Select your Aap" class="chosen-select required" name="apk_id[]" id="apk_id" style="width:350px;" tabindex="2" multiple>
									<!--<option value="0">ALL</option>-->
									<?php 
									FillMultiple("mem_appkey", "apk_id", "apk_title", "notification_apps", "apk_id", "nt_id", $nt_id);
									/*if($_SESSION['UType']==3){
										//FillSelected2("mem_appkey", "apk_id", "apk_title", $apk_id, "mem_id='".$member_id."'");
										FillSelected("mem_appkey", "apk_id", "apk_title", $apk_id);
									}
									else{
										FillSelected("mem_appkey", "apk_id", "apk_title", $apk_id);
									}*/
									?>
								</select>
							<?php } ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Site</label>
							<div id="ddlSites" class="col-lg-10 col-md-9">
								<!--<select data-placeholder="Select Site" name="site_id[]" id="site_id" class="chosen-select" style="width:350px;" multiple>-->
								<select data-placeholder="Select Site" name="site_id[]" id="site_id" class="chosen-select" style="width:350px;" multiple>
								<?php 
									if($_SESSION['UType']>3){
										if($_REQUEST['action']==2){
											FillMultiple("mem_sites WHERE mem_id='".$_SESSION['UserID']."'", "site_id", "site_title", "notification_sites", "site_id", "nt_id", $nt_id);
										}
										else{
											FillMultiple("mem_sites WHERE mem_id='".$_SESSION['UserID']."'", "site_id", "site_title", "notification_sites", "site_id", "nt_id", $nt_id);
										}
									}
									else{
										if($_REQUEST['action']==2){
											FillMultiple("mem_sites", "site_id", "site_title", "notification_sites", "site_id", "nt_id", $nt_id);
										}
									}
								?>
									<!--<option value="0">All</option>-->
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Beacons</label>
							<div id="ddlBeacons" class="col-lg-10 col-md-9">
								<select data-placeholder="Select Beacons" name="msb_id[]" id="msb_id" class="chosen-select" style="width:350px;" multiple>
								<?php 
									if($_SESSION['UType']>3){
										if($_REQUEST['action']==2){
											FillMultiple("msite_beacons", "msb_id", "msb_name", "notification_beacons", "msb_id", "nt_id", $nt_id);
										}
										else{
										}
									}
									else{
										if($_REQUEST['action']==2){
											FillMultiple("msite_beacons", "msb_id", "msb_name", "notification_beacons", "msb_id", "nt_id", $nt_id);
										}
										else{
										}
									}
									
								?>
									<!--<option value="0">All</option>-->
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Zone</label>
							<div class="col-lg-10 col-md-9">
								<select data-placeholder="Select Zone" class="chosen-select required" name="zone_id" id="zone_id" style="width:350px;" tabindex="2">
								<!--<select data-placeholder="Choose a Country..." class="chosen-select" style="width:350px;" tabindex="2">-->
									<!--<option value=""></option>-->
									<option value="0"></option>
									<?php 
										FillSelected("notification_zones", "nzones_id", "nzones_name", @$nzone_id);
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Schedule</label>
							<div class="col-lg-10 col-md-9">
								<div class="days_sch">
									<div class="head">Days</div>
									<div class="head row_pad">Start</div>
									<div class="head row_pad">End</div>
								</div>
							<?php
								$ds = mysql_query("SELECT * FROM days ORDER BY day_id");
								while($dr=mysql_fetch_object($ds)){
									$ds1 = mysql_query("SELECT * FROM notification_schedule WHERE day_id=".$dr->day_id." AND nt_id=".$nt_id);
									if(mysql_num_rows($ds1)>0){
										$dr1 = mysql_fetch_object($ds1);
										$nt_starttime = $dr1->nt_starttime;
										$nt_endtime = $dr1->nt_endtime;
									} else{
										$nt_starttime = "";
										$nt_endtime = "";
									}
							?>
								<div class="days_sch">
									<div class="head"><?php print($dr->day_name);?><input type="hidden" name="day_id[]" value="<?php print($dr->day_id);?>"></div>
									<div class="cnt"><input type="text" name="nt_starttime[]" value="<?php print($nt_starttime);?>" class="form-control form-cascade-control" style="padding-left:6px;"></div>
									<div class="cnt"><input type="text" name="nt_endtime[]" value="<?php print($nt_endtime);?>" class="form-control form-cascade-control" style="padding-left:6px;"></div>
								</div>
							<?php
								}
							?>
							</div>
						</div>
					<!--</form>-->
					</div>
				</div>
				<div id="step-3">
					<h2 class="StepTitle">Target Audiance</h2>
					<div class="panel">
						<!--<div class="panel-heading">
							<h3 class="panel-title">Target Audiance </h3>
						</div>-->
						<div class="panel-body">
							<!--<form name="frm" id="frm" method="post" action="<?php //print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form">-->
							<div class="form-group">
								<label for="site_login" class="col-lg-2 col-md-3 control-label">Age:</label>
								<div class="col-lg-10 col-md-9">
									<div class="col-lg-1">
										<input type="checkbox" name="age_idALL" onClick="setAll2(this, 'age_id[]');" value="0"> All
									</div>
									<div class="col-lg-8">
							<?php 
								$ageRange ="";
								$r1=mysql_query("SELECT * FROM lov_age ORDER BY age_min");
								while($rw1=mysql_fetch_object($r1)){
									$ageRange = $rw1->age_min;
									if(!empty($rw1->age_max)){
										$ageRange .= " - " . $rw1->age_max;
									}
									else{
										$ageRange .= " +";
									}
									$checked = "";
									if (in_array($rw1->age_id, $age_ids)) {
										$checked = "checked";
									}
									print('<input type="checkbox" name="age_id[]" value="'.$rw1->age_id.'" '.$checked.'> '.$ageRange.' &nbsp;&nbsp;&nbsp;&nbsp;');
								}
							?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="site_fname" class="col-lg-2 col-md-3 control-label">Gender:</label>
								<div class="col-lg-10 col-md-9"> 
									<div class="col-lg-1">
										<input type="checkbox" name="gender_idALL" onClick="setAll2(this, 'gender_id[]');" value=""> All
									</div>
									<div class="col-lg-8">
							<?php 
								$r2=mysql_query("SELECT * FROM lov_gender ORDER BY gender_id");
								while($rw2=mysql_fetch_object($r2)){
									$checked = "";
									if (in_array($rw2->gender_id, $gender_ids)) {
										$checked = "checked";
									}
									print('<input type="checkbox" name="gender_id[]" value="'.$rw2->gender_id.'" '.$checked.'> '.$rw2->gender_name.' &nbsp;&nbsp;&nbsp;&nbsp;');
								}
							?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="site_fname" class="col-lg-2 col-md-3 control-label">Income:</label>
								<div class="col-lg-10 col-md-9"> 
									<div class="col-lg-1">
										<input type="checkbox" name="income_idALL" onClick="setAll2(this, 'income_id[]');" value="0"> All
									</div>
									<div class="col-lg-8">
							<?php 
								$incRange ="";
								$r3=mysql_query("SELECT * FROM lov_income ORDER BY income_min");
								while($rw3=mysql_fetch_object($r3)){
									$incRange = $rw3->income_min;
									if(!empty($rw3->income_max)){
										$incRange .= " - " . $rw3->income_max;
									}
									else{
										$incRange .= " +";
									}
									$checked = "";
									if (in_array($rw3->income_id, $income_ids)) {
										$checked = "checked";
									}
									print('<input type="checkbox" name="income_id[]" value="'.number_format($rw3->income_id).'" '.$checked.'> '.$incRange.' &nbsp;&nbsp;&nbsp;&nbsp;');
								}
							?>
									</div>
								</div>
							</div>
							<!--</form>-->
						</div>
					</div>
				</div>
				<div id="step-4">
					<h2 class="StepTitle">Finish</h2>
					<div class="panel">
						<div class="col-md-8">
							<!--<div class="panel">-->
								<!--<div class="panel-heading">
									<h3 class="panel-title text-primary"> Demo Panel <span class="pull-right"> <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a> <a href="#" class="panel-close"><i class="fa fa-times"></i></a> </span> </h3>
								</div>-->
								<div class="panel-body panel-border"> 
								<!--<form name="frm" id="frm" method="post" action="<?php //print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form" enctype="multipart/form-data">-->
									<div class="form-group">
										<label class="col-lg-2 col-md-3 control-label">Template</label>
										<div class="col-lg-10 col-md-9">
											<select data-placeholder="Select Template" class="chosen-select" name="tpl_id" id="tpl_id" style="width:350px;" tabindex="2">
												<option value="0"></option>
												<?php 
													FillSelected("templates", "tpl_id", "tpl_name", @$tpl_id);
												?>
											</select>
										</div>
									</div>
									<div class="form-group tplOptions">
										<label for="site_login" class="col-lg-2 col-md-3 control-label">Background Color:</label>
										<div class="col-lg-3 col-md-3">
											<input type="text" class="form-control form-cascade-control input_wid70" name="nt_bgcolor" id="nt_bgcolor" value="<?php @print($nt_bgcolor);?>" placeholder="Background Color">
										</div>
									<!--</div>
									<div class="form-group">-->
										<label for="site_login" class="col-lg-2 col-md-2 control-label">Font Color:</label>
										<div class="col-lg-3 col-md-3">
											<input type="text" class="form-control form-cascade-control input_wid70" name="nt_fontcolor" id="nt_fontcolor" value="<?php @print($nt_fontcolor);?>" placeholder="Font Color">
										</div>
									</div>
									<div class="form-group tplOptions">
										<label for="iFile" class="col-lg-2 col-md-3 control-label">Image:</label>
										<div class="col-lg-6 col-md-6">
											<input type="file" name="iFile" id="iFile" class="form-control form-cascade-control input_wid100" />
										</div>
									</div>
									<div class="form-group">
										<label for="site_login" class="col-lg-2 col-md-3 control-label">Title:</label>
										<div class="col-lg-10 col-md-9">
											<input type="text" class="form-control form-cascade-control input_wid70 required" name="nt_title" id="nt_title" value="<?php @print($nt_title);?>" placeholder="Title">
										</div>
									</div>
									<div class="form-group">
										<label for="site_fname" class="col-lg-2 col-md-3 control-label">Description:</label>
										<div class="col-lg-10 col-md-9"> 
											<input type="text" class="form-control form-cascade-control input_wid70 required" name="nt_details" id="nt_details" value="<?php @print($nt_details);?>" placeholder="Decsription">
										</div>
									</div>
									<div class="form-group">
										<label for="site_login" class="col-lg-2 col-md-3 control-label">Facebook:</label>
										<div class="col-lg-6 col-md-6">
											<input type="text" class="form-control form-cascade-control input_wid70" name="nt_url_fb" id="nt_url_fb" value="<?php print($nt_url_fb);?>" placeholder="Facebook URL">
										</div>
									</div>
									<div class="form-group">
										<label for="site_login" class="col-lg-2 col-md-3 control-label">Twitter:</label>
										<div class="col-lg-6 col-md-6">
											<input type="text" class="form-control form-cascade-control input_wid70" name="nt_url_tw" id="nt_url_tw" value="<?php print($nt_url_tw);?>" placeholder="Twitter URL">
										</div>
									</div>
									<div class="form-group">
										<label for="site_login" class="col-lg-2 col-md-3 control-label">Google+:</label>
										<div class="col-lg-6 col-md-6">
											<input type="text" class="form-control form-cascade-control input_wid70" name="nt_url_gp" id="nt_url_gp" value="<?php print($nt_url_gp);?>" placeholder="Google Plus URL">
										</div>
									</div>
									<div class="form-group">
										<label for="site_login" class="col-lg-2 col-md-3 control-label">Pintrest:</label>
										<div class="col-lg-6 col-md-6">
											<input type="text" class="form-control form-cascade-control input_wid70" name="nt_url_pn" id="nt_url_pn" value="<?php print($nt_url_pn);?>" placeholder="Pintrest URL">
										</div>
									</div>
								<!--</form>-->
								</div>
								<!-- /panel body --> 
							<!--</div>-->
						</div>
						<div class="col-md-4" style="padding-left:0px;">
							<div class="panel-heading" style="padding-left:0px;">
								<h3 class="panel-title text-primary"> Preview </h3>
							</div>
							<div class="panel-body nopadding"> Please select the template first to see its preview 
								<div id="preview" style="width:340px; height:640px; border:1px solid #999; overflow:hidden; /*overflow-y: scroll;*/">
									
								</div>
							</div>
						</div>
					</div>
						
						
					
					
						<!--<div class="panel-body">
							<div class="form-group">
								<label for="site_login" class="col-lg-2 col-md-3 control-label">Title:</label>
								<div class="col-lg-10 col-md-9">
									<input type="text" class="form-control form-cascade-control input_wid70 required" name="nt_title" id="nt_title" value="<?php print($nt_title);?>" placeholder="Title">
								</div>
							</div>
							<div class="form-group">
								<label for="site_fname" class="col-lg-2 col-md-3 control-label">Description:</label>
								<div class="col-lg-10 col-md-9"> 
									<input type="text" class="form-control form-cascade-control input_wid70 required" name="nt_details" id="nt_details" value="<?php print($nt_details);?>" placeholder="Decsription">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 col-md-3 control-label">Template</label>
								<div class="col-lg-10 col-md-9">
									<select data-placeholder="Select Template" class="chosen-select" name="tpl_id" id="tpl_id" style="width:350px;" tabindex="2">
										<option value="0"></option>
										<?php 
											//FillSelected("templates", "tpl_id", "tpl_name", @$tpl_id);
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="iFile" class="col-lg-2 col-md-3 control-label">Image:</label>
								<div class="col-lg-6 col-md-6">
									<input type="file" name="iFile" id="iFile" class="form-control form-cascade-control input_wid100" />
								</div>
							</div>
							<div class="form-group">
								<label for="site_login" class="col-lg-2 col-md-3 control-label">Background Color:</label>
								<div class="col-lg-3 col-md-3">
									<input type="text" class="form-control form-cascade-control input_wid70" name="nt_bgcolor" id="nt_bgcolor" value="<?php print($nt_bgcolor);?>" placeholder="Background Color">
								</div>
								<label for="site_login" class="col-lg-2 col-md-2 control-label">Font Color:</label>
								<div class="col-lg-3 col-md-3">
									<input type="text" class="form-control form-cascade-control input_wid70 
									" name="nt_fontcolor" id="nt_fontcolor" value="<?php //print($nt_fontcolor);?>" placeholder="Font Color">
								</div>
							</div>
							
						</div>-->
					</div>
				</div>
			</div>
				<input type="hidden" name="nt_id" id="nt_id" value="<?php print($nt_id);?>">
				<input type="hidden" name="nt_image" id="nt_image" value="<?php print($nt_image);?>">
				<input type="hidden" name="template_id" id="template_id" value="<?php print($tpl_id);?>">
				<input type="hidden" name="btnSubmit" value="1">
			</form>
			<!-- End SmartWizard Content --> 
			
		</div>
		<!-- /panel body --> 
	</div>
</div>
