<div class="col-md-12">
	<div class="panel panel-dat">
		<div class="panel-heading">
			<h3 class="panel-title text-primary"> Basic  Wizard <span class="pull-right"> <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a> <a href="#" class="panel-close"><i class="fa fa-times"></i></a> </span> </h3>
		</div>
		<div class="panel-body nopadding"> 
			
			<!-- Smart Wizard -->
			<div id="wizard" class="swMain">
				<ul>
					<li><a href="#step-1">
						<label class="stepNumber">1</label>
						<span class="stepDesc"> Introduction </span> </a></li>
					<li><a href="#step-2">
						<label class="stepNumber">2</label>
						<span class="stepDesc"> Registration </span> </a></li>
					<li><a href="#step-3">
						<label class="stepNumber">3</label>
						<span class="stepDesc"> Terms </span> </a></li>
					<li><a href="#step-4">
						<label class="stepNumber">4</label>
						<span class="stepDesc"> Finish </span> </a></li>
				</ul>
				<div id="step-1">
					<h2 class="StepTitle">Introduction</h2>
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"> Cascade Template </h3>
						</div>
						<div class="panel-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud <span class="label bg-purple">exercitation</span> ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in <span class="label bg-pink">voluptate</span> velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
							<ul type="disk">
								<li>List 1</li>
								<li>List 2</li>
								<li>List 3</li>
								<li>List 4</li>
								<li>List 5</li>
							</ul>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in <span class="label bg-success">voluptate</span> velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in <span class="label bg-warning">reprehenderit</span> in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
							<p><span class="label bg-seagreen">Lorem ipsum</span> dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <span class="label bg-info">Lorem ipsum</span> dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
						</div>
					</div>
				</div>
				<div id="step-2">
					<h2 class="StepTitle">Enter Details</h2>
					<form class="form-horizontal cacade-forms" method="post" action="#" name="signup_form" id="signup_form" novalidate>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Email</label>
							<div class="col-lg-10 col-md-9">
								<input type="text" class="form-control form-cascade-control input-small" name="email" id="email">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Username</label>
							<div class="col-lg-10 col-md-9">
								<input type="text" class="form-control form-cascade-control input-small" name="username" id="username">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Password</label>
							<div class="col-lg-10 col-md-9">
								<input type="text" class="form-control form-cascade-control input-small" name="password" id="password">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Confirm Password</label>
							<div class="col-lg-10 col-md-9">
								<input type="text" class="form-control form-cascade-control input-small" name="confirmPassword" id="confirmPassword">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Site URL</label>
							<div class="col-lg-10 col-md-9">
								<input type="text" class="form-control form-cascade-control input-small" name="url" id="url">
							</div>
						</div>
					</form>
				</div>
				<div id="step-3">
					<h2 class="StepTitle">Terms</h2>
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"> Make a Note </h3>
						</div>
						<div class="panel-body">
							<ul type="disk">
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
								<li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
								<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text </li>
								<li>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it</li>
								<li>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
								<li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Suspendisse ultricies orci nec mi pretium lobortis.Suspendisse ultricies orci nec mi pretium lobortis.</li>
								<li>Duis iaculis massa aliquam eros mollis iaculis.</li>
								<li>Suspendisse consectetur erat eget urna adipiscing dignissim.In ut ante fringilla, dignissim metus nec, luctus justo.</li>
								<li>Quisque vitae lectus elementum, molestie ligula non, scelerisque libero.</li>
								<li>Sed ac eros tempor, scelerisque turpis vitae, ullamcorper tellus.Sed sodales ipsum molestie ultrices sagittis.</li>
								<li>Sed sodales ipsum molestie ultrices sagittis.</li>
								<li>Nulla bibendum dui eu risus bibendum consectetur.</li>
								<li>In ut ante fringilla, dignissim metus nec, luctus justo.Ut id nisl suscipit, congue leo vel, rhoncus eros.</li>
								<li>Quisque in purus a arcu tincidunt consequat.</li>
								<li>Suspendisse ultricies orci nec mi pretium lobortis.Duis laoreet lorem vel faucibus pulvinar.Duis laoreet lorem vel faucibus pulvinar.</li>
								<li>Nunc vitae leo sit amet sem egestas accumsan.</li>
								<li>In sed sapien faucibus, venenatis magna sed, posuere sem.</li>
								<li>Mauris id ipsum sed lorem tincidunt venenatis.Ut id nisl suscipit, congue leo vel, rhoncus eros.</li>
								<li>Aenean dapibus elit vel orci auctor, ut euismod sem laoreet.In ut ante fringilla, dignissim metus nec, luctus justo.</li>
								<li>Nunc sagittis diam quis massa tempor congue.</li>
								<li>Ut id nisl suscipit, congue leo vel, rhoncus eros.Duis laoreet lorem vel faucibus pulvinar.</li>
								<li>Morbi a purus consequat, aliquam odio non, pellentesque ante.In ut ante fringilla, dignissim metus nec, luctus justo.</li>
								<li>Duis laoreet lorem vel faucibus pulvinar.</li>
								<li>In quis odio vel enim sodales suscipit id at leo</li>
							</ul>
						</div>
					</div>
				</div>
				<div id="step-4">
					<h2 class="StepTitle">Finish</h2>
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title"> Thanks for your Registration! </h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
								Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
						</div>
					</div>
				</div>
			</div>
			<!-- End SmartWizard Content --> 
			
		</div>
		<!-- /panel body --> 
	</div>
</div>
