
<!-- Load JS here for Faster site load =============================-->
<script src="../partials/js/jquery-1.10.2.min.js"></script>
<script src="../partials/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="../partials/js/less-1.5.0.min.js"></script>
<script src="../partials/js/jquery.ui.touch-punch.min.js"></script>
<script src="../partials/js/bootstrap.min.js"></script>
<script src="../partials/js/bootstrap-select.js"></script>
<script src="../partials/js/bootstrap-switch.js"></script>
<script src="../partials/js/jquery.tagsinput.js"></script>
<script src="../partials/js/jquery.placeholder.js"></script>
<script src="../partials/js/bootstrap-typeahead.js"></script>
<script src="../partials/js/application.js"></script>
<script src="../partials/js/moment.min.js"></script>
<script src="../partials/js/jquery.dataTables.min.js"></script>
<script src="../partials/js/jquery.sortable.js"></script>
<script type="text/javascript" src="../partials/js/jquery.gritter.js"></script>
<script src="../partials/js/jquery.nicescroll.min.js"></script>
<script src="../partials/js/skylo.js"></script>
<script src="../partials/js/prettify.min.js"></script>
<script src="../partials/js/jquery.noty.js"></script>
<script src="../partials/js/bic_calendar.js"></script>
<script src="../partials/js/jquery.accordion.js"></script>
<script src="../partials/js/theme-options.js"></script>
<script src="../partials/js/failsafe.js"></script>

<script src="../partials/js/bootstrap-progressbar.js"></script>
<script src="../partials/js/bootstrap-progressbar-custom.js"></script>
<script src="../partials/js/bootstrap-colorpicker.min.js"></script>
<script src="../partials/js/bootstrap-colorpicker-custom.js"></script>
