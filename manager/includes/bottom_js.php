<!-- Load JS here for Faster site load =============================-->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/less-1.5.0.min.js"></script>
<script src="js/jquery.ui.touch-punch.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/bootstrap-switch.js"></script>
<script src="js/jquery.tagsinput.js"></script>
<script src="js/jquery.placeholder.js"></script>
<script src="js/bootstrap-typeahead.js"></script>
<script src="js/application.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/jquery.sortable.js"></script>
<script type="text/javascript" src="js/jquery.gritter.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/skylo.js"></script>
<script src="js/prettify.min.js"></script>
<script src="js/jquery.noty.js"></script>
<script src="js/bic_calendar.js"></script>
<script src="js/jquery.accordion.js"></script>
<script src="js/theme-options.js"></script>
<script src="js/failsafe.js"></script>
<script src="js/chosen.jquery.js"></script>

<script src="js/bootstrap-progressbar.js"></script>
<script src="js/bootstrap-progressbar-custom.js"></script>
<script src="js/bootstrap-colorpicker.min.js"></script>
<script src="js/bootstrap-colorpicker-custom.js"></script>

<!-- Page Script File  =============================-->
<script src="js/validate.js"></script>
<script language="javascript">
$(document).ready(function(){
	$("#frm").validate();
});
</script>
<script src="js/validation-custom.js"></script>

<!-- Page Script File  =============================-->
<script src="js/jquery.smartWizard.js"></script>
<script src="js/smartWizard-custom.js"></script>
<script src="js/jquery.pulsate.min.js"></script>
<script src="js/forms-custom.js"></script>

<!-- Core Jquery File  =============================-->
<script src="js/core.js"></script>

<!-- START :: For Notification Page -->
<script language="javascript">
$(document).ready(function(e) {
	getBeacons();
	$("#apk_id").change(function(e) {
		getSites();
		getBeacons();
	});
	
	$("#site_id").change(function(e) {
		getBeacons();
	});
	
	function getSites(){
		$.ajax({
			type: "POST",
			url: "includes/ajax.php",
			data: {apk_id:$("#apk_id").val(), fillSites: 1, nt_id: $("#nt_id").val()}
		})
		.done(function( msg ) {
			//$("#ddlSites").html(msg);
			$("#site_id").html(msg);
			//$("#site_id").trigger("liszt:updated");
		});
	}
	function getBeacons(){
		//alert($("#site_id").val());
		$.ajax({
			type: "POST",
			url: "includes/ajax.php",
			data: {apk_id:$("#apk_id").val(), site_id:$("#site_id").val(), fillBeacons: 1, nt_id: $("#nt_id").val()}
		})
		.done(function( msg ) {
			$("#ddlBeacons").html(msg);
			//$("#msb_id").html(msg);
			//$("#ddlBeacons").trigger('liszt:updated');
			$(document).on("load","#ddlBeacons",function(e){
				$("#ddlBeacons").chosen();
				//$("#msb_id").trigger("chosen:updated");
				$("#msb_id").trigger('liszt:updated');
			});
		});
	}
	
	/*$(".buttonFinish").click(function(e) {
		if($(".buttonFinish").is(":enabled")){
			alert("Submit");
		}
	});*/
});

//$("#apk_id[]").change(function(e) {
//$("#apk_id[]").select(function(e) {
	//alert($("select[name='apk_id[]']").val());
	/*$.ajax({
		type: "POST",
		url: "includes/ajax.php",
		data: { apk_id: $(this).val()}
	})
	.done(function( msg ) {
		alert( "Data Saved: " + msg );
	});*/
//});
</script>
<!-- END :: For Notification Page -->