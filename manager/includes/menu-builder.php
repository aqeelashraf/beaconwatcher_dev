<?php
	$pageName = basename($_SERVER["PHP_SELF"]);
	$posName = strpos($pageName, '.php');
	$pageName = (substr($pageName, 0, $posName));
	$pg = $pageName;
	$c = 'active';
if($_SESSION["UType"]==4){
?>
<li class='<?php echo $pg=='index'?$c:'';?>' ><a href='index.php' data-original-title='Dashboard'><i class='fa fa-dashboard'></i><span class='hidden-minibar'> Dashboard</span></a></li>
<li class='submenu '><a class='dropdown' href='#' data-original-title='Management'><i class='fa fa-indent'></i><span class='hidden-minibar'> Management <span class='badge bg-primary pull-right'>4</span></span></a>
	<ul>
		<li class='<?php echo ($pg=='manage_apps' || $pg=='manage_users')?$c:'';?>' ><a href='manage_apps.php' data-original-title='My Apps'><i class='fa fa-gears'></i><span class='hidden-minibar'> My Apps</span></a></li>
		<li class='<?php echo ($pg=='manage_sites' || $pg=='manage_beacons' || $pg=='beacon_contents')?$c:'';?>' ><a href='manage_sites.php' data-original-title='My Sites'><i class='fa fa-gears'></i><span class='hidden-minibar'> My Venues</span></a></li>
		<li class='<?php echo ($pg=='manage_all_beacons')?$c:'';?>' ><a href='manage_all_beacons.php' data-original-title='All Beacons'><i class='fa fa-gears'></i><span class='hidden-minibar'> All Beacons</span></a></li>
		<li class='<?php echo ($pg=='manage_notifications')?$c:'';?>' ><a href='manage_notifications.php' data-original-title='Notifications'><i class='fa fa-gears'></i><span class='hidden-minibar'> Notifications</span></a></li>
	</ul>
</li>
<?php 
}
elseif($_SESSION["UType"]==3){
?>
<li class='<?php echo $pg=='index'?$c:'';?>' ><a href='index.php' data-original-title='Dashboard'><i class='fa fa-dashboard'></i><span class='hidden-minibar'> Dashboard</span></a></li>
<li class='submenu '><a class='dropdown' href='#' data-original-title='Management'><i class='fa fa-indent'></i><span class='hidden-minibar'> Management <span class='badge bg-primary pull-right'>4</span></span></a>
	<ul>
		<li class='<?php echo ($pg=='manage_apps' || $pg=='manage_users')?$c:'';?>' ><a href='manage_apps.php' data-original-title='My Apps'><i class='fa fa-gears'></i><span class='hidden-minibar'> My Apps</span></a></li>
		<li class='<?php echo ($pg=='manage_sites' || $pg=='manage_beacons' || $pg=='beacon_contents')?$c:'';?>' ><a href='manage_sites.php' data-original-title='My Sites'><i class='fa fa-gears'></i><span class='hidden-minibar'> My Venues</span></a></li>
		<li class='<?php echo ($pg=='manage_all_beacons')?$c:'';?>' ><a href='manage_all_beacons.php' data-original-title='All Beacons'><i class='fa fa-gears'></i><span class='hidden-minibar'> All Beacons</span></a></li>
		<li class='<?php echo ($pg=='manage_notifications')?$c:'';?>' ><a href='manage_notifications.php' data-original-title='Notifications'><i class='fa fa-gears'></i><span class='hidden-minibar'> Notifications</span></a></li>
	</ul>
</li>
<?php 
}
else{
?>
<li class='<?php echo $pg=='index'?$c:'';?>' ><a href='index.php' data-original-title='Dashboard'><i class='fa fa-dashboard'></i><span class='hidden-minibar'> Dashboard</span></a></li>
<li class='submenu '><a class='dropdown' href='#' data-original-title='Management'><i class='fa fa-indent'></i><span class='hidden-minibar'> Management <span class='badge bg-primary pull-right'>6</span></span></a>
	<ul>
		<li class='<?php echo ($pg=='manage_users')?$c:'';?>' ><a href='manage_users.php' data-original-title='Users'><i class='fa fa-gears'></i><span class='hidden-minibar'> Users</span></a></li>
		<li class='<?php echo ($pg=='manage_apps')?$c:'';?>' ><a href='manage_apps.php' data-original-title='Apps'><i class='fa fa-gears'></i><span class='hidden-minibar'> Apps</span></a></li>
		<li class='<?php echo ($pg=='manage_all_beacons')?$c:'';?>' ><a href='manage_all_beacons.php' data-original-title='All Beacons'><i class='fa fa-gears'></i><span class='hidden-minibar'> All Beacons</span></a></li>
		<li class='<?php echo ($pg=='manage_sites' || $pg=='manage_beacons' || $pg=='beacon_contents')?$c:'';?>' ><a href='manage_sites.php' data-original-title='Sites'><i class='fa fa-gears'></i><span class='hidden-minibar'> Sites</span></a></li>
		<li class='<?php echo ($pg=='manage_templates')?$c:'';?>' ><a href='manage_templates.php' data-original-title='Templates'><i class='fa fa-gears'></i><span class='hidden-minibar'> Templates</span></a></li>
		<li class='<?php echo ($pg=='manage_categories')?$c:'';?>' ><a href='manage_categories.php' data-original-title='Templates'><i class='fa fa-gears'></i><span class='hidden-minibar'> Categories</span></a></li>
	</ul>
</li>
<?php 
}
?>