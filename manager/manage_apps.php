<?php 
include('includes/header.php'); 
$formHead = "Add New";
$strMSG = "";
$class = "";
$bcrum = "Apps";
if($_SESSION["UType"] == 3){
	$bcrum = "My Apps";
}
//if(!$_SESSION["UType"] == 3){
	if(isset($_REQUEST['member_id'])){
		$_SESSION['member_id'] = $_REQUEST['member_id'];
	}
	else{
		if(!isset($_SESSION['member_id'])){
			$_SESSION['member_id']=0;
		}
	}
//}

if(isset($_REQUEST['action'])){
	if(isset($_REQUEST['btnAdd'])){
		$apk_id = getMaximum("mem_appkey","apk_id");
		$appKey = genAppKey($_SESSION['member_id'], $_REQUEST['apk_id'], $_REQUEST['apk_title']);
		$prefixUUID = $apk_id.$_SESSION['member_id'];
		$appUUID = generateUUID($prefixUUID);
		//mysql_query("INSERT INTO mem_appkey(apk_id, mem_id, apk_key, apk_title, apk_details, status_id, apk_dateadded, apk_uuid) VALUES(".$apk_id.", '".$_SESSION['member_id']."', '".$appKey."', '".dbStr($_REQUEST['apk_title'])."', '".dbStr($_REQUEST['apk_details'])."', '1', '".date("Y-m-d")."', '".dbStr($_REQUEST['apk_uuid'])."')") or die(mysql_error());
		mysql_query("INSERT INTO mem_appkey(apk_id, mem_id, apk_key, apk_title, apk_details, status_id, apk_dateadded, apk_uuid) VALUES(".$apk_id.", '".$_SESSION['member_id']."', '".$appKey."', '".dbStr($_REQUEST['apk_title'])."', '".dbStr($_REQUEST['apk_details'])."', '1', '".date("Y-m-d")."', '".dbStr($appUUID)."')") or die(mysql_error());
		header("Location: ".$_SERVER['PHP_SELF']."?op=1");
	}
	elseif(isset($_REQUEST['btnUpdate'])){
		$udtQuery = "UPDATE mem_appkey SET apk_title='".dbStr($_REQUEST['apk_title'])."', apk_details='".dbStr($_REQUEST['apk_details'])."', apk_uuid='".dbStr($_REQUEST['apk_uuid'])."', apk_lastupdated='".date("Y-m-d")."' WHERE apk_id=".$_REQUEST['apk_id'];
		mysql_query($udtQuery) or die(mysql_error());
		header("Location: ".$_SERVER['PHP_SELF']."?op=2");
	}
	elseif($_REQUEST['action']==2){
		$rsM = mysql_query("SELECT * FROM mem_appkey WHERE apk_id=".$_REQUEST['apk_id']);
		if(mysql_num_rows($rsM)>0){
			$rsMem = mysql_fetch_object($rsM);
			$apk_id = $rsMem->apk_id;
			$apk_title = $rsMem->apk_title;
			$apk_details = $rsMem->apk_details;
			$apk_key = $rsMem->apk_key;
			$apk_uuid = $rsMem->apk_uuid;
			$formHead = "Update Info";
		}
	}
	else{
		$apk_id = "";
		$apk_title = "";
		$apk_details = "";
		$apk_key = "";
		$apk_uuid = "";
		$formHead = "Add New";
	}
}
if(isset($_REQUEST['show'])){
	$rsM = mysql_query("SELECT * FROM mem_appkey WHERE apk_id=".$_REQUEST['apk_id']);
	if(mysql_num_rows($rsM)>0){
		$rsMem = mysql_fetch_object($rsM);
		$apk_id = $rsMem->apk_id;
		$apk_title = $rsMem->apk_title;
		$apk_details = $rsMem->apk_details;
		$apk_key = $rsMem->apk_key;
		$apk_uuid = $rsMem->apk_uuid;
		$apk_dateadded = $rsMem->apk_dateadded;
		$apk_lastupdated = $rsMem->apk_lastupdated;
		$formHead = "Update Info";
	}
}
//--------------Button Active--------------------
if(isset($_REQUEST['btnActive'])){
	for($i=0; $i<count($_REQUEST['chkstatus']); $i++){
		mysql_query("UPDATE mem_appkey SET status_id = 1 WHERE apk_id = ".$_REQUEST['chkstatus'][$i]);
	}
	$class = "alert alert-success";
	$strMSG = "Record(s) updated successfully";
}
//--------------Button InActive--------------------
if(isset($_REQUEST['btnInactive'])){
	for($i=0; $i<count($_REQUEST['chkstatus']); $i++){
		mysql_query("UPDATE mem_appkey SET status_id = 0 WHERE apk_id = ".$_REQUEST['chkstatus'][$i]);
	}
	$class = "alert alert-success";
	$strMSG = "Record(s) updated successfully";
}
//--------------Button Delete--------------------
if(isset($_REQUEST['btnDelete'])){
	for($i=0; $i<count($_REQUEST['chkstatus']); $i++){
		//mysql_query("DELETE FROM mem_sites WHERE site_id = ".$_REQUEST['chkstatus'][$i]);
		//mysql_query("UPDATE mem_appkey SET apk_del=1 WHERE apk_id=".$_REQUEST['chkstatus'][$i]) or die(mysql_query());
	}
	$class = "alert alert-success";
	$strMSG = "Record(s) deleted successfully";
}
if(isset($_REQUEST['op'])){
	switch ($_REQUEST['op']) {
		case 1:
			$class = "alert alert-success";
			$strMSG = "Record Added Successfully";
			break;
		case 2:
			$strMSG = " Record Updated Successfully";
			$class = "alert alert-success";
			break;
		case 4:
			$class = "notification success";
			$strMSG = "Please Select Checkbox to Add or Subtract Credits";
			break;
	}
}
?>
			<!-- /header -->
			<div class="row">
				<div class="col-mod-12">
					<ul class="breadcrumb">
						<li><a href="index.php">Dashboard</a></li>
						<!--<li><a href="template.php">Basic Template</a></li>-->
						<li class="active"><?php print($bcrum);?></li>
					</ul>
					<div class="form-group hiddn-minibar pull-right">
						<!--<input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />
						<span class="input-icon fui-search"></span>--> </div>
					<h3 class="page-header"> Apps Management <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
					<blockquote class="page-information hidden">
						<p> <b>Apps Management</b> is the section where you can Manage and register your APPs. </p>
					</blockquote>
				</div>
			</div>
			<div class="<?php print($class);?>"><?php print($strMSG);?></div>
		<?php if(isset($_REQUEST['action'])){ ?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-cascade">
						<div class="panel-heading">
							<h3 class="panel-title">
								<?php print($formHead);?>
								<!--<span class="pull-right">
									<a  href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
									<a  href="#"  class="panel-close"><i class="fa fa-times"></i></a>
								</span>-->
							</h3>
						</div>
						<div class="panel-body">
							<form name="frm" id="frm" method="post" action="<?php print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form">
								<div class="form-group">
									<label for="site_login" class="col-lg-2 col-md-3 control-label">App Name:</label>
									<div class="col-lg-10 col-md-9">
										<input type="text" class="form-control form-cascade-control input_wid70 required" name="apk_title" id="apk_title" value="<?php print($apk_title);?>" placeholder="App Name">
									</div>
								</div>
							<?php if($_REQUEST['action']>1){ ?>
								<div class="form-group">
									<label for="site_login" class="col-lg-2 col-md-3 control-label">App UUID:</label>
									<div class="col-lg-10 col-md-9">
										<input type="text" class="form-control form-cascade-control input_wid70" name="apk_uuid" id="apk_uuid" value="<?php print($apk_uuid);?>" placeholder="App UUID">
									</div>
								</div>
							<?php } ?>
								<div class="form-group">
									<label for="site_fname" class="col-lg-2 col-md-3 control-label">Details:</label>
									<div class="col-lg-10 col-md-9"> 
										<input type="text" class="form-control form-cascade-control input_wid70 required" name="apk_details" id="apk_details" value="<?php print($apk_details);?>" placeholder="Details">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail1" class="col-lg-2 col-md-3 control-label">&nbsp;</label>
									<div class="col-lg-10 col-md-9">
									<?php if($_REQUEST['action']==1){ ?>
										<button type="submit" name="btnAdd" class="btn btn-primary btn-animate-demo">Submit</button>
									<?php } else{ ?>
										<button type="submit" name="btnUpdate" class="btn btn-primary btn-animate-demo">Submit</button>
									<?php } ?>
										<button type="button" name="btnCancel" class="btn btn-default btn-animate-demo" onclick="javascript: window.location='<?php print($_SERVER['PHP_SELF']);?>';">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php } elseif(isset($_REQUEST['show'])){ ?>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-cascade">
						<div class="panel-heading">
							<h3 class="panel-title">
								Details
							</h3>
						</div>
						<div class="panel-body">
							<form name="frm" id="frm" method="post" action="<?php print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form">
								<div class="form-group">
									<label for="site_login" class="col-lg-2 col-md-3 control-label">App Name:</label>
									<div class="col-lg-10 col-md-9 padTop7"><?php print($apk_title);?></div>
								</div>
								<div class="form-group">
									<label for="site_login" class="col-lg-2 col-md-3 control-label">App UUID:</label>
									<div class="col-lg-10 col-md-9 padTop7"><?php print($apk_uuid);?></div>
								</div>
								<div class="form-group">
									<label for="site_fname" class="col-lg-2 col-md-3 control-label">Details:</label>
									<div class="col-lg-10 col-md-9 padTop7"><?php print($apk_details);?></div>
								</div>
								<div class="form-group">
									<label for="site_fname" class="col-lg-2 col-md-3 control-label">App Key:</label>
									<div class="col-lg-10 col-md-9 padTop7"><?php print($apk_key);?></div>
								</div>
								<div class="form-group">
									<label for="site_fname" class="col-lg-2 col-md-3 control-label">Date Added:</label>
									<div class="col-lg-10 col-md-9 padTop7"><?php print($apk_dateadded);?></div>
								</div>
								<div class="form-group">
									<label for="site_fname" class="col-lg-2 col-md-3 control-label">Last Updated:</label>
									<div class="col-lg-10 col-md-9 padTop7"><?php print($apk_lastupdated);?></div>
								</div>
								<div class="form-group">
									<label for="inputEmail1" class="col-lg-2 col-md-3 control-label">&nbsp;</label>
									<div class="col-lg-10 col-md-9">
										<button type="button" name="btnBack" class="btn btn-default btn-animate-demo" onclick="javascript: window.location='<?php print($_SERVER['PHP_SELF']);?>';">Back</button>
									</div>
								</div>					
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php } else{ ?>
			<div class="row">
				<div class="col-md-12">
				<?php print(generateUUID("413"));?>
			<?php if($_SESSION["UType"]==1 || $_SESSION["UType"]==2){?>
				<div>
					<form method="post" name="frmMember" action="<?php print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>">
					Client:  
					<select name="member_id" id="member_id" class="chosen-select" style="width:150px;" onChange="javascript: frmMember.submit();">
						<option value="0">All</option>
						<?php FillSelected2("members", "mem_id", "mem_fname", $_SESSION['member_id'], "utype_id=3");?>
					</select>
					</form>
				</div>
			<?php } ?>
					<div class="panel">
						<div class="panel-heading text-primary">
							<h3 class="panel-title"><i class="fa fa-sitemap"></i> Apps 
								<span class="pull-right" style="width:auto;">
						<?php if($_SESSION["UType"] <= 3){ ?>
									<div style="float:right;"><a href="<?php print($_SERVER['PHP_SELF']."?action=1");?>" title="Add New"><i class="fa fa-plus"></i> Add New</a></div>
						<?php } ?>
								</span> 
							</h3>
						</div>
						<div class="panel-body">
						<form name="frm" id="frm" method="post" action="<?php print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form">
							<table class="table users-table table-condensed table-hover table-striped" >
								<thead>
									<tr>
										<th class="visible-lg"><input type="checkbox" name="chkAll" onClick="setAll();"></th>
										<th class="visible-lg">Name</th>
										<th class="visible-lg">Key</th>
										<th class="visible-lg">UUID</th>
										<th class="visible-lg">Date Added</th>
										<th class="visible-lg">Last Updated</th>
										<th>Status</th>
										<th width="140">Action</th>
									</tr>
								</thead>
								<tbody>
								<?php
									if($_SESSION["UType"] > 3){
										$Query = "SELECT k.*, s.status_name FROM mem_appkey AS k LEFT OUTER JOIN status As s ON s.status_id=k.status_id WHERE k.apk_id=".$_SESSION['my_apk_id'];
									}
									else{
										if($_SESSION['member_id']>0){
											$Query = "SELECT k.*, s.status_name FROM mem_appkey AS k LEFT OUTER JOIN status As s ON s.status_id=k.status_id WHERE k.mem_id=".$_SESSION['member_id'];
										}
										else{
											$Query = "SELECT k.*, s.status_name FROM mem_appkey AS k LEFT OUTER JOIN status As s ON s.status_id=k.status_id";
										}
									}
									//print($Query);
									$counter=0;
									$limit = 25;
									$start = $p->findStart($limit); 
									$count = mysql_num_rows(mysql_query($Query)); 
									$pages = $p->findPages($count, $limit); 
									$rs = mysql_query($Query." LIMIT ".$start.", ".$limit);
									if(mysql_num_rows($rs)>0){
										while($row=mysql_fetch_object($rs)){	
											$counter++;
								?>
									<tr>
										<td class="visible-lg"><input type="checkbox" name="chkstatus[]" value="<?php print($row->apk_id);?>" /></td>
										<td class="visible-lg"><?php print($row->apk_title);?> </td>
										<td class="visible-lg"><?php print($row->apk_key);?></td>
										<td class="visible-lg"><?php print($row->apk_uuid);?></td>
										<td class="visible-lg"><?php print($row->apk_dateadded);?></td>
										<td><?php print($row->apk_lastupdated);?></td>
										<td><?php print($row->status_name);?></td>
										<td><!--<button type="button" class="btn btn-success"><i class="fa fa-envelope"></i></button>-->
											<button type="button" class="btn btn-info" title="View Details" onclick="javascript: window.location='<?php print($_SERVER['PHP_SELF']."?show=1&apk_id=".$row->apk_id);?>';"><i class="fa fa-eye"></i></button>
											<?php if($_SESSION["UType"] <= 3){ ?>
											<button type="button" class="btn btn-warning" title="Edit" onclick="javascript: window.location='<?php print($_SERVER['PHP_SELF']."?action=2&apk_id=".$row->apk_id);?>';"><i class="fa fa-edit"></i></button>
											<button type="button" class="btn btn-success" title="App Users" onclick="javascript: window.location='manage_users.php?apk_id=<?php print($row->apk_id);?>';"><i class="fa fa-user-md"></i></button>
											<?php } ?>
										</td>
									</tr>
								<?php
										}
									}
									else{
										print('<tr><td colspan="100%" align="center">No record found!</td></tr>');
									}
								?>
								</tbody>
							</table>
							<?php if($counter > 0) {?>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><?php print("Page <b>".$_GET['page']."</b> of ".$pages);?></td>
										<td align="right">
										<?php	
											$next_prev = $p->nextPrev($_GET['page'], $pages, '');
											print($next_prev);
										?>
										</td>
									</tr>
								</table>
							<?php }?>
							<?php if($_SESSION["UType"] <= 3){ ?>
							<?php if($counter > 0) {?>
                                 <input type="submit" name="btnActive" value="Active" class="btn btn-primary btn-animate-demo">
                                 <input type="submit" name="btnInactive" value="In Active" class="btn btn-danger btn-animate-demo">
							<?php }?>
							<?php }?>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
			<!-- Demo Panel -->
		</div>
		<!-- /.content --> 
		
		<!-- .right-sidebar -->
		<?php include("includes/rightsidebar.php")?>
	</div>
	<!-- /.right-sidebar --> 
	
	<!-- /rightside bar --> 
	
</div>
<!-- /.box-holder -->
</div>
<!-- /.site-holder -->

<?php include("includes/bottom_js.php")?>
</body>
</html>
<?php include("../lib/closeCon.php"); ?>