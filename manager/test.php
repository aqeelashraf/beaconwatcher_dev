<?php include('includes/header.php'); ?>
			<!-- /header -->
			<div class="row">
				<div class="col-mod-12">
					<ul class="breadcrumb">
						<li><a href="index.php">Dashboard</a></li>
						<li><a href="template.php">Basic Template</a></li>
						<li class="active">BreadCrumb</li>
					</ul>
					<div class="form-group hiddn-minibar pull-right">
						<input type="text" class="form-control form-cascade-control nav-input-search" size="20" placeholder="Search through site" />
						<span class="input-icon fui-search"></span> </div>
					<h3 class="page-header"> Basic Template <i class="fa fa-info-circle animated bounceInDown show-info"></i> </h3>
					<blockquote class="page-information hidden">
						<p> <b>Template Page</b> is the basic page where you can add more pages according to your requirements easily within this division. </p>
					</blockquote>
				</div>
			</div>
			
			<!-- Demo Panel -->
			<div class="row">
				<div class="col-md-8">
					<div class="panel panel-cascade">
						<div class="panel-heading">
							<h3 class="panel-title text-primary"> Demo Panel <span class="pull-right"> <a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a> <a href="#" class="panel-close"><i class="fa fa-times"></i></a> </span> </h3>
						</div>
						<div class="panel-body panel-border"> This is a basic template page to quick start your project. 
						<form name="frm" id="frm" method="post" action="<?php print($_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']);?>" class="form-horizontal" role="form" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-lg-2 col-md-3 control-label">Template</label>
								<div class="col-lg-10 col-md-9">
									<select data-placeholder="Select Template" class="chosen-select" name="tpl_id" id="tpl_id" style="width:350px;" tabindex="2">
										<option value="0"></option>
										<?php 
											FillSelected("templates", "tpl_id", "tpl_name", @$tpl_id);
										?>
									</select>
								</div>
							</div>
							<div class="form-group tplOptions">
								<label for="site_login" class="col-lg-2 col-md-3 control-label">Background Color:</label>
								<div class="col-lg-3 col-md-3">
									<input type="text" class="form-control form-cascade-control input_wid70" name="nt_bgcolor" id="nt_bgcolor" value="<?php @print($nt_bgcolor);?>" placeholder="Background Color">
								</div>
							<!--</div>
							<div class="form-group">-->
								<label for="site_login" class="col-lg-2 col-md-2 control-label">Font Color:</label>
								<div class="col-lg-3 col-md-3">
									<input type="text" class="form-control form-cascade-control input_wid70" name="nt_fontcolor" id="nt_fontcolor" value="<?php @print($nt_fontcolor);?>" placeholder="Font Color">
								</div>
							</div>
							<div class="form-group tplOptions">
								<label for="iFile" class="col-lg-2 col-md-3 control-label">Image:</label>
								<div class="col-lg-6 col-md-6">
									<input type="file" name="iFile" id="iFile" class="form-control form-cascade-control input_wid100" />
								</div>
							</div>
							<div class="form-group">
								<label for="site_login" class="col-lg-2 col-md-3 control-label">Title:</label>
								<div class="col-lg-10 col-md-9">
									<input type="text" class="form-control form-cascade-control input_wid70 required" name="nt_title" id="nt_title" value="<?php @print($nt_title);?>" placeholder="Title">
								</div>
							</div>
							<div class="form-group">
								<label for="site_fname" class="col-lg-2 col-md-3 control-label">Description:</label>
								<div class="col-lg-10 col-md-9"> 
									<input type="text" class="form-control form-cascade-control input_wid70 required" name="nt_details" id="nt_details" value="<?php @print($nt_details);?>" placeholder="Decsription">
								</div>
							</div>
						</form>
						</div>
						<!-- /panel body --> 
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel-heading">
						<h3 class="panel-title text-primary"> Heading </h3>
					</div>
					<div class="panel-body nopadding"> This is a basic template page to quick start your project. 
						<div id="preview" style="width:340px; height:640px; border:1px solid #999; overflow-y: scroll;">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.content --> 
		
		<!-- .right-sidebar -->
		<?php include("includes/rightsidebar.php")?>
	</div>
	<!-- /.right-sidebar --> 
	
	<!-- /rightside bar --> 
	
</div>
<!-- /.box-holder -->
</div>
<!-- /.site-holder -->
<?php include("includes/bottom_js.php")?>
<script language="javascript" src="js/ajaxfileupload.js"></script>
<script language="javascript" src="js/template_script.js"></script>
</body>
</html>